﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public enum GameStates { MainMenu, Game, Pause, Defeat, Win }

public class GameManager : GenericSingletonClass<GameManager>
{

    public const string WinScene = "WinScene";
    public const string DefScene = "DefeatScene";
    public const string m_GameScene = "TestScene_KurzovViveS";
    public const string m_MainMenu = "MainMenu";
    //public string PouseScene;

    //PlayerController PlayerController;
    private Character m_Player;
    private Lawa m_Lava;
    public GameStates m_CurrentState { get; private set; }
    //private bool isPause = false;

    public Character Player { get { return m_Player; } }
    public Lawa Lava { get { return m_Lava; } }

    public static Action StartLoading = () => { };

    private Coroutine m_LoadingCoroutine;
    private AsyncOperation m_AsyncLoading;

    public override void Awake()
    {
        base.Awake();

        if(SceneManager.GetActiveScene().name == m_MainMenu)
        {
            m_CurrentState = GameStates.MainMenu;
        }
        else
        {
            m_CurrentState = GameStates.Game;
        }
    }

    private void Start()
    {
        Player.SetEnergy(3);
    }

    public void LoadDefeat()
    {
        SceneManager.LoadScene(DefScene);
        m_CurrentState = GameStates.Defeat;
    }


    public void LoadWinGame()
    {
        SceneManager.LoadScene(WinScene);
        m_CurrentState = GameStates.Win;

    }

    public void LoadGameLevel(int index)
    {
        if(m_LoadingCoroutine == null)
        {
            m_LoadingCoroutine = StartCoroutine(LoadGameAsync(index));
        }
    }

    private IEnumerator LoadGameAsync(int index)
    {
        StartLoading.Invoke();

        m_AsyncLoading = SceneManager.LoadSceneAsync(m_GameScene, LoadSceneMode.Single);

        yield return m_AsyncLoading;

        m_CurrentState = GameStates.Game;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(m_MainMenu);
        m_CurrentState = GameStates.MainMenu;
    }

    public void PauseGame()
    {
        //AudioManager.Instance.Pause();
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        //AudioManager.Instance.Resume();
        Time.timeScale = 1f;
    }

    public void LoadRestart()
    {
        //AudioManager.Instance.StopAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        m_CurrentState = GameStates.Game;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(LoadGameAsync(0));
        }
    }

    public void SetPlayer(Character player)
    {
        if (m_Player == null)
        {
            m_Player = player;
        }
    }

    public void SetLava(Lawa lava)
    {
        if (m_Lava == null)
        {
            m_Lava = lava;
        }
    }

    public static GameStates CurrentState { get { return Instance.m_CurrentState; } }

    public float GetLoadingProgress()
    {
        if (m_AsyncLoading != null)
            return m_AsyncLoading.progress;
        else
            return 0f;
    }
}
