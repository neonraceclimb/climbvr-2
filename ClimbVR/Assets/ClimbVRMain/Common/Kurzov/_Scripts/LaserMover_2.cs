﻿using UnityEngine;
using System.Collections;

public class LaserMover_2 : MonoBehaviour {

	public GameObject platform; // reference to the platform to move

	public GameObject[] myWaypoints; // array of all the waypoints

	[Range(0.0f, 10.0f)] // create a slider in the editor and set limits on moveSpeed
	public float moveSpeed = 5f; // enemy move speed
	public float waitAtWaypointTime = 1f; // how long to wait at a waypoint before _moving to next waypoint

    // private variables

    Transform _transform;
	int _myWaypointIndex = 0;		// used as index for My_Waypoints
	float _moveTime;
	bool _moving = true;
    private bool _stoping;
    private Vector3 m_TargetPos;
    private Vector3 m_DefaultPos;
    private bool _stopMoving;

    // Use this for initialization
    void Start () {
		_transform = platform.transform;
        m_DefaultPos = _transform.position;
		_moveTime = 0f;
		_moving = false;
        _stopMoving = false;
    }

	// game loop
	void Update () {
		// if beyond _moveTime, then start moving
		if (Time.time >= _moveTime) {
			Movement();
		}
	}

	void Movement() {
		// if there isn't anything in My_Waypoints
		if ((myWaypoints.Length != 0) && (_moving)) {

			// move towards waypoint
			_transform.position = Vector3.MoveTowards(_transform.position, m_TargetPos, moveSpeed * Time.deltaTime);

			// if the enemy is close enough to waypoint, make it's new target the next waypoint
			if(Vector3.Distance(m_TargetPos, _transform.position) <= 0.001f) {
                if (_stoping)
                {
                    _moving = false;
                    _stoping = false;
                }

                if (_stopMoving)
                {
                    _stoping = true;
                    _stopMoving = false;
                    m_TargetPos = m_DefaultPos;
                }
                else
                {
                    m_TargetPos = GetNextTarget();
                    _moveTime = Time.time + waitAtWaypointTime;
                }
			}
		}
	}

    private Vector3 GetNextTarget()
    {
        _myWaypointIndex++;

        if (_myWaypointIndex >= myWaypoints.Length)
        {
            _myWaypointIndex = 0;
        }

        return myWaypoints[_myWaypointIndex].transform.position;
    }

    public void StartMoving()
    {
        _moving = true;
        m_TargetPos = GetNextTarget();
    }

    public void StopMoving()
    {
        _stopMoving = true;
    }

    public bool IsMoving { get { return _moving; } }
}