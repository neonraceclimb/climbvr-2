﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBarrier : LazerBase
{

	public Transform BlockBarier1;
	public Transform BlockBarier2;

	[SerializeField]
	private float m_ActiveTime;
	[SerializeField]
	private float m_WaitTime;
    [SerializeField]
    private float m_ReloadTime;

    private const string m_MatRimCol = "_RimColor";
    private int m_RimPropID;

    [SerializeField] private MeshRenderer[] m_Fires;

    protected override void Start()
    {
        base.Start();
        StartCoroutine(Barier());
        m_RimPropID = Shader.PropertyToID(m_MatRimCol);
    }

    private IEnumerator Barier()
    {
        float distance = Vector3.Distance(BlockBarier1.position, BlockBarier2.position);

        while(true)
        {
            yield return StartCoroutine(Reload(true));

            yield return StartCoroutine(Active(distance));

            yield return StartCoroutine(Reload(false));

            yield return StartCoroutine(Wait());
        }
    }

    private IEnumerator Reload(bool reload)
    {
        int i = 0;

        //VisualizeRay(BlockBarier1.position, BlockBarier2.position, 0.05f);
        while (i < m_Fires.Length)
        {
            EnableFire(i, reload);

            i++;
            yield return new WaitForSeconds(m_ReloadTime);
        }
        //DisableVisualization();
    }

    protected void EnableFire(int i, bool reload)
    {

    }

    private IEnumerator Active(float distance)
    {
        float t = 0f;
        EnableFire(0, false);
        VisualizeRay(BlockBarier1.position, BlockBarier2.position);
        while (t <= m_ActiveTime)
        {
            CastLazer(BlockBarier1.position, BlockBarier2.position, distance);

            t += Time.deltaTime;
            yield return null;
        }
        DisableVisualization();
    }

    private IEnumerator Wait()
    {
        float t = 0f;

        while (t <= m_WaitTime)
        {
            DisableVisualization();

            t += Time.deltaTime;
            yield return null;
        }
    }
}
