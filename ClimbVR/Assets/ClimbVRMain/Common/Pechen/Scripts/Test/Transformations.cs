﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformations : MonoBehaviour {

    public Transform m_Transform;
    public Transform m_OtherTransform;
    Vector3? enterPos = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //transform.position = transform.TransformPoint(transform.localPosition);
            print("World Pos: " + transform.InverseTransformPoint(m_Transform.position) + "   Local Pos: " + transform.position);
        }


        if(Input.GetKeyDown(KeyCode.Space))
        {
            enterPos = transform.InverseTransformPoint(m_OtherTransform.position);
        }

        if (enterPos != null)
        {
            m_OtherTransform.position = transform.TransformPoint((Vector3)enterPos);
        }
    }
}
