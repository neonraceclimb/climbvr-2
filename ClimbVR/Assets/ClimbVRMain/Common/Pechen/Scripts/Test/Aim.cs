﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{

    [SerializeField] private float m_RotationSpeed;
    [SerializeField] private float m_ShowHideSpeed;
    [SerializeField] private AnimationCurve m_ShowHideCurve;
    [SerializeField] private float m_InitialScale;

    private Coroutine m_CurrentCoroutine;
    private float m_CurveTime;
    private Vector3 m_StartScale;

    public void Show(float delay)
    {
        m_StartScale = Vector3.one * m_InitialScale;
        transform.rotation = transform.parent.rotation;
        m_CurrentCoroutine = StartCoroutine(ShowCoroutine(delay / 2f));
    }

    public void Hide(float delay)
    {
        StartCoroutine(HideCoroutine(delay / 2f));
    }

    void Update()
    {
        transform.Rotate(Vector3.forward, m_RotationSpeed * Time.deltaTime);
    }


    private IEnumerator ShowCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        m_CurveTime = 0f;

        while (m_CurveTime < 1f)
        {
            m_CurveTime += Time.deltaTime / m_ShowHideSpeed;
            transform.localScale = m_StartScale * m_ShowHideCurve.Evaluate(m_CurveTime);
            yield return null;
        }

        transform.localScale = m_StartScale;
    }

    private IEnumerator HideCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (m_CurrentCoroutine != null) StopCoroutine(m_CurrentCoroutine);

        while (m_CurveTime > 0f)
        {
            m_CurveTime -= Time.deltaTime / m_ShowHideSpeed;
            transform.localScale = m_StartScale * m_ShowHideCurve.Evaluate(m_CurveTime);
            yield return null;
        }

        transform.localScale = Vector3.zero;

        m_CurrentCoroutine = null;

        DestroyImmediate(this.gameObject);
    }
}
