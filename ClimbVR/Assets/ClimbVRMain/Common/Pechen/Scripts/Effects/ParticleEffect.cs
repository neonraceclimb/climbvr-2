﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect {

    public ParticleSystem ps { get; private set; }
    public GameObject go { get; private set; }

    public ParticleEffect(ParticleSystem ps, GameObject go)
    {
        this.ps = ps;
        this.go = go;
    }
}
