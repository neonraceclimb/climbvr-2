﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour {

    private static EffectsData m_Effects;
    private static List<ParticleEffect> particleEffects;

    private void Awake()
    {
        particleEffects = new List<ParticleEffect>();
    }

    private void Start()
    {
        m_Effects = GetComponent<EffectsData>();
    }

    void Update () {
        particleEffects.RemoveAll(effect => effect.go == null);

        foreach (ParticleEffect effect in particleEffects)
        {
            if(!effect.ps.isPlaying)
            {
                DestroyEffect(effect.go);
            }
        }
	}

    private void DestroyEffect(GameObject effectGO)
    {
        DestroyImmediate(effectGO);
    }

    public static void Spawn(Vector3 position, Quaternion rotation, Effects effect, Transform parent)
    {
        GameObject instance = Instantiate(m_Effects.GetEffect(effect), position, rotation, parent);
        ParticleSystem pSystem = instance.GetComponentInChildren<ParticleSystem>();
        particleEffects.Add(new ParticleEffect(pSystem, instance));
    }
}
