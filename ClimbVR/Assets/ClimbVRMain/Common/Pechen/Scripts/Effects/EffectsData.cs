﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Effects { Effect_1, Effect_2 }

public class EffectsData : MonoBehaviour {

    [SerializeField] private GameObject effectOne;
    [SerializeField] private GameObject effectTwo;
	
	public GameObject GetEffect(Effects effect)
    {
        switch (effect)
        {
            case Effects.Effect_1:
                return effectOne;
            case Effects.Effect_2:
                return effectTwo;
            default:
                Debug.LogWarning("Can't find effect!");
                return null;
        }
    }
}
