﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableComponentTrigger : MonoBehaviour {

    [SerializeField] private MonoBehaviour[] m_Components;
    [SerializeField] private GameObject[] m_GameObjects;

    private bool m_CurrentState = true;

    private void Awake()
    {
        Setup();
        Active(false);
        m_CurrentState = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ActiveTrigger")
        {
            Active(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ActiveTrigger")
        {
            Active(false);
        }
    }

    private void Active(bool active)
    {
        if(m_CurrentState != active)
        {
            for (int i = 0; i < m_Components.Length; i++)
            {
                m_Components[i].enabled = active;
            }
            for (int i = 0; i < m_GameObjects.Length; i++)
            {
                m_GameObjects[i].SetActive(active);
            }

            m_CurrentState = active;
        }
    }

    private void Setup()
    {
        BoxCollider col = gameObject.AddComponent<BoxCollider>();
        col.size = Vector3.one;

        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
    }
}
