﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    [SerializeField] private GameObject m_ShieldPrefab;
    [SerializeField] private Transform m_ShiledPosition;
    [SerializeField] private float m_ShowHideDuration;
    [SerializeField] private AnimationCurve m_ShowHideCurve;


    private float m_EffectTime = 0f;
    private Material m_Material;
    private GameObject m_Shield;
    private Coroutine m_ShowHideCoroutine;

	public void SetupShield()
    {
        if (m_Shield != null)
        {
            StopCoroutine(m_ShowHideCoroutine);
            DestroyImmediate(m_Shield);
        }

        m_Shield = Instantiate(m_ShieldPrefab, m_ShiledPosition);
        m_Material = m_Shield.GetComponent<MeshRenderer>().material;

        m_ShowHideCoroutine = StartCoroutine(ShowShield());
    }

    private IEnumerator ShowShield()
    {
        m_EffectTime = 0f;

        while(m_EffectTime <= 1f)
        {
            m_EffectTime += Time.unscaledDeltaTime / m_ShowHideDuration;
            m_Material.SetFloat("_Emergence", Mathf.Clamp01(1f - m_ShowHideCurve.Evaluate(m_EffectTime)));
            yield return null;
        }

        m_EffectTime = 1f;
    }

    public void DisableShield()
    {
        if(m_Shield)
        {
            if (m_ShowHideCoroutine != null) StopCoroutine(m_ShowHideCoroutine);
            m_ShowHideCoroutine = StartCoroutine(HideShield());
        }
    }

    private IEnumerator HideShield()
    {
        while (m_EffectTime >= 0f)
        {
            m_EffectTime -= Time.unscaledDeltaTime / m_ShowHideDuration;
            m_Material.SetFloat("_Emergence", Mathf.Clamp01(1f - m_ShowHideCurve.Evaluate(m_EffectTime)));
            yield return null;
        }

        DestroyImmediate(m_Shield);
        m_ShowHideCoroutine = null;
    }
}
