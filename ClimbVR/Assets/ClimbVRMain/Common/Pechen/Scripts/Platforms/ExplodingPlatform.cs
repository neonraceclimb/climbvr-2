﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingPlatform : PlatformBase {

    [SerializeField] private float m_Speed;
    [SerializeField] private float m_Distance;
    [SerializeField] private AnimationCurve m_Levitation;
    [SerializeField] private AnimationCurve m_FlyCurve;
    [SerializeField] private AnimationCurve m_ExplosionCurve;
    [SerializeField] private float m_ExplosionDuration;
    [SerializeField] private float m_ExplosionRadius;

    private Vector3 m_ExplosionPosition;
    private Vector3 m_StartPosition;

    private Vector3 m_DeltaMovement;
    private Vector3 m_TempPosition;
    private Coroutine m_FlyCoroutine;
    private bool m_PlayerHoldingOnPlatform;
    private float m_Radius = 0f;


    private bool m_Idle;

    // Use this for initialization
    public override void Awake()
    {
        base.Awake();
        m_Idle = true;
        m_StartPosition = transform.position;
        m_ExplosionPosition = transform.position + Vector3.up * m_Distance;
    }


    // Update is called once per frame
    void Update () {
		if(m_Idle)
        {
            float t = Mathf.Repeat(Time.time * 0.1f, 1f);
            transform.position = new Vector3(transform.position.x, m_StartPosition.y + m_Levitation.Evaluate(t), transform.position.z);
        }
	}

    public override void OnTriggerDown(HandsController controller)
    {
        base.OnTriggerDown(controller);
        m_TempPosition = transform.position;
        m_Idle = false;
        if(m_FlyCoroutine == null)
        {
            m_FlyCoroutine = StartCoroutine(ExplodingFly(controller));
        }
        m_PlayerHoldingOnPlatform = true;
    }

    public override void OnPressTrigger(HandsController controller)
    {
        base.OnPressTrigger(controller);
        m_DeltaMovement = transform.position - m_TempPosition;//send delta pos to player
        m_Player.AddDeltaMovement(m_DeltaMovement);
        m_TempPosition = transform.position;
    }

    public override void OnTriggerUp(HandsController controller)
    {
        base.OnTriggerUp(controller);
        m_PlayerHoldingOnPlatform = false;
    }

    private IEnumerator ExplodingFly(HandsController controller)
    {
        float t = 0f;
        float dist = Vector3.Distance(m_StartPosition, m_ExplosionPosition);
        m_Speed /= dist;

        m_StartPosition = transform.position;

        while (t < 1f)
        {
            t += (m_PlayerHoldingOnPlatform ? Time.deltaTime : Time.fixedDeltaTime) * m_Speed;
            transform.position = Vector3.Lerp(m_StartPosition, m_ExplosionPosition, m_FlyCurve.Evaluate(t));
            if(m_PlayerHoldingOnPlatform)
            {
                yield return null;
            }
            else
            {
                yield return new WaitForFixedUpdate();
            }
        }

        controller.PressTriggerUp();
        StartCoroutine(Explode());
    }

    private IEnumerator Explode()
    {
        float t = 0f;

        while(t < 1f)
        {
            m_Radius = m_ExplosionRadius * m_ExplosionCurve.Evaluate(t);
            Collider[] cols = Physics.OverlapSphere(transform.position, m_Radius);
            foreach (Collider col in cols)
            {
                if (col.tag == "Player")
                {
                    m_Player.SetDamage("Explosion Platform");
                }
            }

            t += Time.deltaTime / m_ExplosionDuration;

            yield return null;
        }

        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, .3f, 0f);
        Gizmos.DrawLine(transform.position, transform.position + Vector3.up * m_Distance);

        Vector3 endPoint = transform.position + Vector3.up * m_Distance;
        Gizmos.DrawSphere(endPoint, .05f);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(m_ExplosionPosition, m_Radius);
    }
}
