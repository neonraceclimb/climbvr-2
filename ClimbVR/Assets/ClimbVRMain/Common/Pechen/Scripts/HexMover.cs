﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMover : MonoBehaviour
{

    [SerializeField] private float m_MinDist;
    [SerializeField] private float m_MaxDist;
    [SerializeField] private float m_PrizmaInscribedCircleRadius;
    [SerializeField] private int m_EdgesCount;
    [SerializeField] private float m_AttractionForce;
    [SerializeField] private LayerMask m_PrizmaLayer;
    [SerializeField] private Vector3 m_RayOffset1;
    [SerializeField] private Vector3 m_RayOffset2;

    private Vector3 m_CurrentNormal = Vector3.back;
    private readonly Vector3 m_XZ = new Vector3(1f, 0f, 1f);
    private float m_StepAngle;
    private Vector3 m_RadiusVectorFromPrizma;

    private float m_HalfSideLength;
    private float m_ChangeEdgeDelta;
    private float m_CircumscribedRadius;

    private float m_HalfSideLengthScalable;
    private float m_ChangeEdgeDeltaScalable;
    private float m_CircumscribedRadiusScalable;
    private float m_PrizmaInscribedCircleRadiusScalable;

    private float m_SideSign;
    private Vector3 m_ProjectionToPrizm;

    private bool m_Init;

    public static Vector3 HexForce { get; private set; }
    
    private void Awake()
    {
        m_Init = false;
    }

    void Initialize(Vector3 playerPosition)
    {
        m_StepAngle = 360f / m_EdgesCount;
        CalculateConstants(m_PrizmaInscribedCircleRadius);

        m_HalfSideLengthScalable = m_HalfSideLength;
        m_ChangeEdgeDeltaScalable = m_ChangeEdgeDelta;
        m_CircumscribedRadiusScalable = m_CircumscribedRadius;
        m_PrizmaInscribedCircleRadiusScalable = m_PrizmaInscribedCircleRadius;

        CalculateProjectionToSide(playerPosition);
    }

    private void CalculateConstants(float inscribedRadius)
    {
        m_HalfSideLength = inscribedRadius * Mathf.Tan(m_StepAngle / 2f * Mathf.Deg2Rad);
        m_ChangeEdgeDelta = m_MinDist * Mathf.Tan(m_StepAngle * Mathf.Deg2Rad);
        m_CircumscribedRadius = inscribedRadius / Mathf.Cos(m_StepAngle / 2f * Mathf.Deg2Rad);
    }

    private void RecalculateScalableValues(Vector3 playerPosition)
    {
        float scale = GetDistanceToLine(transform.position.MultiplyCW(m_XZ), transform.position.MultiplyCW(m_XZ)
            + new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x), playerPosition.MultiplyCW(m_XZ)) / (m_PrizmaInscribedCircleRadius + m_MinDist);

        m_HalfSideLengthScalable = m_PrizmaInscribedCircleRadius * scale * Mathf.Tan(m_StepAngle / 2f * Mathf.Deg2Rad);
        m_ChangeEdgeDeltaScalable = m_MinDist * scale * Mathf.Tan(m_StepAngle * Mathf.Deg2Rad);
        m_CircumscribedRadiusScalable = (m_PrizmaInscribedCircleRadius * scale) / Mathf.Cos(m_StepAngle / 2f * Mathf.Deg2Rad);
        m_PrizmaInscribedCircleRadiusScalable = m_PrizmaInscribedCircleRadius * scale;
    }

    public void HexMove(ref Vector3 position, ref Quaternion rotation, ref Vector3 rbVelocity, bool isFixedUpdate)//refactor
    {
        if (!m_Init)
        {
            Initialize(position);
            m_Init = true;
        }
        else
        {
            ChangeNormal();
            RecalculateScalableValues(position);
            CalculateProjectionToSide(position);
            CalculateSideSign(position);
            CheckGround( position,  rotation);

            rotation = GetRotation();
            CalcualateForce(rotation);
            if (CheckGround(position, rotation))
            {
                position = ClampPositionHex(position, ref rbVelocity, isFixedUpdate, rotation);
            }
        }
    }

    private Vector3 ClampPositionHex(Vector3 playerPosition, ref Vector3 rbVelocity, bool isFixedUpdate, Quaternion playerRotation)
    {
        Vector3 RadiusVectorMin = (transform.position + m_CurrentNormal * m_PrizmaInscribedCircleRadius).MultiplyCW(m_XZ);
        Vector3 ProjectionOnPrizm = RadiusVectorMin + Vector3.Project(playerPosition.MultiplyCW(m_XZ) - RadiusVectorMin,
            new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x));
        float distFromSideCenter = Vector3.Distance(RadiusVectorMin, ProjectionOnPrizm);
        if (distFromSideCenter > m_HalfSideLength)
        {
            ProjectionOnPrizm = RadiusVectorMin + new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x) * m_HalfSideLength * m_SideSign;
        }
        Debug.DrawLine(playerPosition.MultiplyCW(m_XZ), ProjectionOnPrizm, Color.yellow);

        if (Vector3.Distance(ProjectionOnPrizm, playerPosition.MultiplyCW(m_XZ)) < m_MinDist)
        {
            Vector3 clampedPos = ProjectionOnPrizm + (playerPosition.MultiplyCW(m_XZ) - ProjectionOnPrizm).normalized * m_MinDist;
            playerPosition = new Vector3(clampedPos.x, playerPosition.y, clampedPos.z);
            if(isFixedUpdate)
            {
                rbVelocity = Quaternion.Inverse(playerRotation) * rbVelocity;
                rbVelocity.z = 0f;
                rbVelocity = playerRotation * rbVelocity;
            }
            else
            {
                rbVelocity = Vector3.zero;
            }
        }
        else if (Vector3.Distance(playerPosition.MultiplyCW(m_XZ), transform.position.MultiplyCW(m_XZ)) < m_PrizmaInscribedCircleRadius)
        {
            Vector3 clampedPos = (playerPosition.MultiplyCW(m_XZ) - transform.position.MultiplyCW(m_XZ)).normalized * m_PrizmaInscribedCircleRadius;
            playerPosition = new Vector3(clampedPos.x, playerPosition.y, clampedPos.z);
        }
        else if(!isFixedUpdate && Vector3.Distance(ProjectionOnPrizm, playerPosition.MultiplyCW(m_XZ)) > m_MaxDist)
        {
            Vector3 clampedPos = ProjectionOnPrizm + (playerPosition.MultiplyCW(m_XZ) - ProjectionOnPrizm).normalized * m_MaxDist;
            playerPosition = new Vector3(clampedPos.x, playerPosition.y, clampedPos.z);
        }
        return playerPosition;
    }

    private Quaternion GetRotation()
    {
        Quaternion from = Quaternion.LookRotation(-m_CurrentNormal);
        Quaternion to = Quaternion.LookRotation(Quaternion.Euler(0f, m_StepAngle * m_SideSign, 0f) * -m_CurrentNormal);
        float t = ClampedLerp(m_HalfSideLengthScalable, m_HalfSideLengthScalable + m_ChangeEdgeDeltaScalable, Vector3.Distance(m_RadiusVectorFromPrizma, m_ProjectionToPrizm));
        return Quaternion.Lerp(from, to, t);
    }

    private void CalculateProjectionToSide(Vector3 playerPosition)
    {
        m_RadiusVectorFromPrizma = (transform.position + m_CurrentNormal * m_PrizmaInscribedCircleRadiusScalable).MultiplyCW(m_XZ);
        m_ProjectionToPrizm = m_RadiusVectorFromPrizma + Vector3.Project(playerPosition.MultiplyCW(m_XZ) - m_RadiusVectorFromPrizma,
            new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x));
    }

    private void CalculateSideSign(Vector3 playerPosition)
    {
        m_SideSign = -Mathf.Sign(Vector3.Cross((playerPosition - m_RadiusVectorFromPrizma).MultiplyCW(m_XZ), m_CurrentNormal).y);
    }

    private void ChangeNormal()
    {
        //CalculateSideSign();
        float projectedDist = Vector3.Distance(m_RadiusVectorFromPrizma, m_ProjectionToPrizm);
        if (projectedDist > m_ChangeEdgeDeltaScalable + m_HalfSideLengthScalable)
        {
            RotateNormalOneStep(m_SideSign);
        }
    }

    private void RotateNormalOneStep(float sign)
    {
        m_CurrentNormal = Quaternion.Euler(0f, m_StepAngle * sign, 0f) * m_CurrentNormal;
    }

    private void CalcualateForce(Quaternion rotation)
    {
        HexForce = rotation * Vector3.forward * m_AttractionForce;
    }

    private bool CheckGround(Vector3 playerPosition, Quaternion playerRotation)
    {
        bool checkOne = CastRayPrizma(playerPosition, playerRotation, m_RayOffset1);

        bool checkTwo = CastRayPrizma(playerPosition, playerRotation, m_RayOffset2);

        return checkOne || checkTwo;
    }

    private bool CastRayPrizma(Vector3 playerPosition, Quaternion playerRotation, Vector3 offset)
    {
        Vector3 rayPos = playerPosition + playerRotation * offset;
        Vector3 rayDir = (transform.position + Vector3.Project(playerPosition - transform.position, transform.up) - playerPosition).normalized;
        Ray ray = new Ray(rayPos, rayDir);
        Debug.DrawRay(rayPos, rayDir, Color.red);

        return Physics.Raycast(ray, float.PositiveInfinity, m_PrizmaLayer);
    }

    #region Gizmos

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawLine(transform.position.MultiplyCW(m_XZ), m_RadiusVectorFromPrizma);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(m_RadiusVectorFromPrizma, m_RadiusVectorFromPrizma + m_CurrentNormal * m_MinDist);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(m_ProjectionToPrizm, 0.02f);

        Gizmos.color = Color.blue;
        Vector3 HalfEdgeFromSideCenter = m_RadiusVectorFromPrizma + new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x) * m_HalfSideLengthScalable;
        Gizmos.DrawLine(m_RadiusVectorFromPrizma, HalfEdgeFromSideCenter);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(HalfEdgeFromSideCenter, HalfEdgeFromSideCenter + new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x) * m_ChangeEdgeDeltaScalable);
        Gizmos.color = Color.blue;
        HalfEdgeFromSideCenter = m_RadiusVectorFromPrizma - new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x) * m_HalfSideLengthScalable;
        Gizmos.DrawLine(m_RadiusVectorFromPrizma, HalfEdgeFromSideCenter);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(HalfEdgeFromSideCenter, HalfEdgeFromSideCenter - new Vector3(m_CurrentNormal.z, 0, -m_CurrentNormal.x) * m_ChangeEdgeDeltaScalable);

        Gizmos.color = Color.cyan;
        float currentAngle = m_StepAngle / 2f * Mathf.Deg2Rad;
        Vector3 p1 = m_CircumscribedRadiusScalable * new Vector3(Mathf.Sin(currentAngle), 0f, Mathf.Cos(currentAngle)) + transform.position.MultiplyCW(m_XZ) + Vector3.up * 0.05f;
        for (int i = 0; i < m_EdgesCount; i++)
        {
            currentAngle += m_StepAngle * Mathf.Deg2Rad;
            Vector3 p2 = m_CircumscribedRadiusScalable * new Vector3(Mathf.Sin(currentAngle), 0f, Mathf.Cos(currentAngle)) + transform.position.MultiplyCW(m_XZ) + Vector3.up * 0.05f;
            Gizmos.DrawLine(p1, p2);
            p1 = p2;
        }
    }
    #endregion

    #region Helper Functions
    private float GetDistanceToLine(Vector3 a, Vector3 b, Vector3 point)
    {
        return Vector3.Cross((b - a).normalized, point - a).magnitude;
    }

    private float Saturate(float x)
    {
        return Mathf.Max(0, Mathf.Min(1, x));
    }

    private float ClampedLerp(float a, float b, float x)
    {
        float t = Saturate((x - a) / (b - a));
        return t;
    }

    #endregion
}


