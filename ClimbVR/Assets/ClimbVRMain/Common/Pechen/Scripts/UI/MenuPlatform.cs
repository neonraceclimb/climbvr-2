﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MenuItemType { MainMenu, Game, Restart, Defeat, Win }

public class MenuPlatform : PlatformBase {

    [SerializeField] private MenuItemType m_Type;
    [SerializeField] private float m_AttractiveSpeed;
    [SerializeField] private AnimationCurve m_SpeedCurve;
    [SerializeField] private MenuItemCore m_Core;
    [SerializeField] private Image m_TipImage;
    [SerializeField] private Text m_TipText;
    [SerializeField] private float m_TipShowDuration;
    [SerializeField] private float m_TipHideDuration;
    [SerializeField] private float m_HideDuration;
    [SerializeField] private Easing.Ease m_HideEase;

    private Coroutine m_CurrentCoroutine;
    private Rigidbody m_Rb;
    private Coroutine m_TipCoroutine;

    private bool m_IsActive;

    private void OnEnable()
    {
        Harpoon.OnAimEnter += EnableTip;
        Harpoon.OnAimExit += DisableTip;
        GameManager.StartLoading += Hide;
    }

    private void OnDisable()
    {
        Harpoon.OnAimEnter -= EnableTip;
        Harpoon.OnAimExit -= DisableTip;
        GameManager.StartLoading -= Hide;
    }

    protected override void Start()
    {
        base.Start();
        m_IsActive = true;
        m_Rb = GetComponent<Rigidbody>();
    }

    public override void OnTriggerDown(HandsController controller)
    {
        if(m_IsActive)
        {
            m_CurrentCoroutine = StartCoroutine(MoveToPlayer(controller));
            EnablePhysics(false);
        }
    }

    public override void OnPressTrigger(HandsController controller)
    {

    }

    public override void OnTriggerUp(HandsController controller)
    {
        if (m_CurrentCoroutine != null)
        {
            StopCoroutine(m_CurrentCoroutine);
        }
        EnablePhysics(true);
    }

    private void Update()
    {
        if(!m_Rb.isKinematic && m_Core)
        {
            m_Rb.AddForce((m_Core.transform.position - transform.position) * m_Core.Force);
        }
    }

    private void EnablePhysics(bool enable)
    {
        m_Rb.isKinematic = !enable;
        m_Rb.useGravity = enable;
    }

    private IEnumerator ShowTip()
    {
        float t = m_TipImage.color.a;

        while (t <= 1f)
        {
            t += Time.deltaTime / m_TipShowDuration;
            m_TipImage.color = new Color(m_TipImage.color.r, m_TipImage.color.g, m_TipImage.color.b, t);
            m_TipText.color = new Color(m_TipText.color.r, m_TipText.color.g, m_TipText.color.b, t);

            yield return null;
        }
    }

    private void EnableTip(Transform transform)
    {
        if(this.transform == transform)
        {
            if (m_TipCoroutine != null) StopCoroutine(m_TipCoroutine);
            m_TipCoroutine = StartCoroutine(ShowTip());
        }
    }

    private void DisableTip(Transform transform)
    {
        if(this.transform == transform)
        {
            if (m_TipCoroutine != null) StopCoroutine(m_TipCoroutine);
            m_TipCoroutine = StartCoroutine(HideTip());
        }
    }

    private IEnumerator HideTip()
    {
        if (m_TipCoroutine != null) StopCoroutine(m_TipCoroutine);

        float t = m_TipImage.color.a;

        while(t >= 0f)
        {
            t -= Time.deltaTime / m_TipHideDuration;
            m_TipImage.color = new Color(m_TipImage.color.r, m_TipImage.color.g, m_TipImage.color.b, t);
            m_TipText.color = new Color(m_TipText.color.r, m_TipText.color.g, m_TipText.color.b, t);

            yield return null;
        }
    }

    private IEnumerator MoveToPlayer(HandsController controller)
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = controller.transform.position + Vector3.forward;
        float dist = Vector3.Distance(startPos, endPos);

        float t = 0f;

        while(t < 1f)
        {
            t += Time.unscaledDeltaTime * m_AttractiveSpeed / dist;

            transform.position = Vector3.Lerp(startPos, endPos, m_SpeedCurve.Evaluate(t));
            yield return null;
        }

        LoadScene();
    }

    private void LoadScene()
    {
        switch (m_Type)
        {
            case MenuItemType.MainMenu:
                GameManager.Instance.LoadMainMenu();
                break;
            case MenuItemType.Game:
                GameManager.Instance.LoadGameLevel(-1);
                break;
            case MenuItemType.Restart:
                GameManager.Instance.LoadRestart();
                break;
            case MenuItemType.Defeat:
                GameManager.Instance.LoadDefeat();
                break;
            case MenuItemType.Win:
                GameManager.Instance.LoadWinGame();
                break;
        }
    }

    private void Hide()
    {
        m_IsActive = false;
        StartCoroutine(HideItem());
    }

    private IEnumerator HideItem()
    {
        Easing.EaseFunc HideFunc = Easing.GetEase(m_HideEase);
        float t = 0f;

        while(t < 1f)
        {
            t += Time.deltaTime / m_HideDuration;

            transform.localScale = Vector3.one - Vector3.one * HideFunc(t);
            yield return null;
        }

        transform.localScale = Vector3.zero;
    }
}