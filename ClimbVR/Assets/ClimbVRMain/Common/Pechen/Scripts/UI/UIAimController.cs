﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAimController : MonoBehaviour
{

    [SerializeField] private GameObject m_AimPrefab;

    private List<GameObject> m_SpawnedAims = new List<GameObject>();
    private Character m_Player;

    public static UIAimController Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }

    private void Start()
    {
        m_Player = GameManager.Instance.Player;
        m_SpawnedAims.Clear();
    }

    public void ShowPossiblePlatforms(List<Transform> platforms, float maxDist = 0f)
    {
        foreach(Transform platform in platforms)
        {
            GameObject aim = Instantiate(m_AimPrefab, platform.position, Quaternion.identity, platform);
            aim.transform.localScale = Vector3.zero;
            m_SpawnedAims.Add(aim);
            float delay = CalculateDelay(maxDist, platform.position);
            aim.GetComponent<Aim>().Show(delay);
        }
    }

    public void HidePossiblePlatforms(float maxDist = 0f)
    {
        foreach(GameObject aim in m_SpawnedAims)
        {
            float delay = CalculateDelay(maxDist, aim.transform.position);
            aim.GetComponent<Aim>().Hide(delay);
        }

        m_SpawnedAims.Clear();
    }

    private float CalculateDelay(float maxDist, Vector3 aimPos)
    {
        float sqrtMaxDist = maxDist * maxDist;
        float sqrPlayerDist = (aimPos - m_Player.transform.position).sqrMagnitude;

        return sqrPlayerDist / sqrtMaxDist;
    }
}
