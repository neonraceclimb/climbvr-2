﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
    [SerializeField] private Slider m_LoadingProgress;
    [SerializeField] private Easing.Ease m_ShowEase;
    [SerializeField] private float m_ShowDuration;

    private Image[] m_GraphicElements;

    private void Start()
    {
        m_GraphicElements = GetComponentsInChildren<Image>();
        SetAlpha(0f);
    }

    private void OnEnable()
    {
        GameManager.StartLoading += Show;
    }

    private void OnDisable()
    {
        GameManager.StartLoading -= Show;
    }

    private void Show()
    {
        StartCoroutine(ShowCoroutine());
    }

    private IEnumerator ShowCoroutine()
    {
        float t = 0f;
        Easing.EaseFunc func = Easing.GetEase(m_ShowEase);

        while (t <= 1f)
        {
            t += Time.deltaTime / m_ShowDuration;

            SetAlpha(func(t));

            yield return null;
        }
    }

    private void Update()
    {
        m_LoadingProgress.value = GameManager.Instance.GetLoadingProgress();
    }

    private void SetAlpha(float alpha)
    {
        for (int i = 0; i < m_GraphicElements.Length; i++)
        {
            Color col = m_GraphicElements[i].color;

            m_GraphicElements[i].color = new Color(col.r, col.g, col.b, alpha);
        }
    }
}
