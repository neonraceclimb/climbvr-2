﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuItemCore : MonoBehaviour {

    [SerializeField] private float m_Speed;
    [SerializeField] private AnimationCurve m_YMovement;
    [SerializeField] private float m_Force;

    public float Force { get { return m_Force; } }

    private float m_StartY;
    private float m_Rand;

	// Use this for initialization
	void Start () {
        m_Rand = Random.value;
        m_StartY = transform.position.y;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float t = Mathf.Repeat(Time.time * m_Speed + m_Rand, 1f);

        transform.position = new Vector3(transform.position.x, m_StartY + m_YMovement.Evaluate(t), transform.position.z);
	}
}
