﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Harpoon : MonoBehaviour
{

    [SerializeField]
    private float m_HarpoonDistance;
    [SerializeField]
    private LineRenderer m_RayLineRend;
    [SerializeField]
    private LayerMask m_Layer;
    [SerializeField]
    Transform m_HarpoonPoint;
    public Transform m_TargetTransform { get; private set; }

    public List<Transform> m_PossiblePlatforms;

    public static Action<Transform> OnAimEnter = (t) => { };
    public static Action<Transform> OnAimExit = (t) => { };

    private Character m_Player;

    private void Awake()
    {
        m_PossiblePlatforms = new List<Transform>();
    }

    private void Start()
    {
        m_Player = GameManager.Instance.Player;
    }

    public void Aim(HandsController hand)
    {

        RaycastHit hit;
        if (Physics.Raycast(m_HarpoonPoint.transform.position, m_HarpoonPoint.transform.forward, out hit, m_HarpoonDistance, m_Layer))
        {

            if(m_TargetTransform == null)
            {
                OnAimEnter.Invoke(hit.transform);
            }
            if(m_PossiblePlatforms.Contains(hit.transform) || GameManager.Instance.m_CurrentState != GameStates.Game)
            {
                m_TargetTransform = hit.transform;
            }
        }
        else
        {
            if(m_TargetTransform != null)
            {
                OnAimExit.Invoke(m_TargetTransform);
            }
            m_TargetTransform = null;
        }

        UpdateView(hand);
    }

    public void SwitchOff()
    {
        OnAimExit.Invoke(m_TargetTransform);

        m_RayLineRend.enabled = false;
        m_TargetTransform = null;

        m_PossiblePlatforms.RemoveAll(p => p != null);
        m_PossiblePlatforms.Clear();

        if (UIAimController.Instance != null)
        {
            UIAimController.Instance.HidePossiblePlatforms(m_HarpoonDistance);
        }
    }

    public void SwitchOn(HandsController hand)
    {
        OnAimEnter.Invoke(m_TargetTransform);

        m_RayLineRend.enabled = true;
        m_RayLineRend.SetPosition(0, Vector3.zero);
        m_RayLineRend.SetPosition(1, Vector3.zero);

        Collider[] possiblePlatformsColliders = Physics.OverlapSphere(hand.transform.position, m_HarpoonDistance, m_Layer);//, LayerMask.NameToLayer("AimPlatform"));
        //print(possiblePlatformsColliders.Length);
        foreach (Collider col in possiblePlatformsColliders)
        {
            HarpoonPlatform platform = col.GetComponent<HarpoonPlatform>();
            if (platform != null && ValidPlatform(platform.transform)) m_PossiblePlatforms.Add(platform.transform);
        }


        if(UIAimController.Instance != null)
        {
            UIAimController.Instance.ShowPossiblePlatforms(m_PossiblePlatforms, m_HarpoonDistance);
        }
    }

    private void UpdateView(HandsController hand)
    {
        Vector3 targetPos;
        if (m_TargetTransform != null)
        {
            targetPos = m_TargetTransform.position;
        }
        else
        {
            targetPos = m_HarpoonPoint.transform.position + m_HarpoonPoint.transform.forward * m_HarpoonDistance;
        }

        m_RayLineRend.SetPosition(0, m_HarpoonPoint.transform.position);
        m_RayLineRend.SetPosition(1, targetPos);
    }

    private bool ValidPlatform(Transform platform)
    {
        float angle = Vector3.Angle(m_Player.transform.forward, platform.forward);
        return angle <= 15f;
    }
}
