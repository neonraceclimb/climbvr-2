﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitCollider : MonoBehaviour {

    private Character m_Player;

    private void Start()
    {
        m_Player = GameManager.Instance.Player;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fragment"))
        {
            m_Player.SetDamage("Fragment");
        }
    }
}
