﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    [SerializeField] private Animator m_Animator;

    public void SetGrab(bool grab)
    {
        ResetAnim();
        m_Animator.SetBool("Grab", grab);
    }

    public void SetShield(bool grab)
    {
        ResetAnim();
        m_Animator.SetBool("Shield", grab);
        m_Animator.SetBool("Lazer", grab);
    }

    public void SetAim(bool grab)
    {
        ResetAnim();
        m_Animator.SetBool("Lazer", grab);

    }

    public void SetIdle(bool grab)
    {
        ResetAnim();
        m_Animator.SetBool("Idle", grab);
    }

    private void ResetAnim()
    {
        m_Animator.SetBool("Shield", false);
        m_Animator.SetBool("Lazer", false);
        m_Animator.SetBool("Grab", false);
        m_Animator.SetBool("Idle", false);
    }
}
