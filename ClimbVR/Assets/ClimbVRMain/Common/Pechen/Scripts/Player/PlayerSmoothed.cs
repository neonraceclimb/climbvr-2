﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSmoothed : MonoBehaviour {

    [SerializeField] private Character m_Player;
    [SerializeField] private float m_SmoothedSpeed;
    [SerializeField] private AnimationCurve m_MuscleCurve;
    [SerializeField] private float m_AverageLifting;

    public Vector3 DeltaMovement { get; private set; }

    private Vector3 m_PrevPos;

	// Use this for initialization
	void Start () {
        m_PrevPos = transform.position;

    }
	
	// Update is called once per frame
	void Update () {

        if(m_Player.m_CurrentState == PlayerStates.GrabLedge || m_Player.m_CurrentState == PlayerStates.HarpoonFly)
        {
            SmoothMove();
        }

        CalculateDeltaMovement();

    }

    private void FixedUpdate()
    {
        if (m_Player.m_CurrentState == PlayerStates.FreeMove || m_Player.m_CurrentState == PlayerStates.Aim)
        {
            SmoothMove();
        }
    }

    private void CalculateDeltaMovement()
    {
        DeltaMovement = transform.position - m_PrevPos;

        m_PrevPos = transform.position;
    }

    private void SmoothMove()
    {
        float dist = Vector3.Distance(transform.position, m_Player.transform.position);
        float t = 1f - Mathf.Clamp01(dist / m_AverageLifting);

        float muscles = Mathf.Pow((m_MuscleCurve.Evaluate(t) * 5f), 2f);

        transform.position = Vector3.Lerp(transform.position, m_Player.transform.position, Time.deltaTime * m_SmoothedSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, m_Player.transform.rotation, Time.deltaTime * m_SmoothedSpeed * 8f);
    }
}
