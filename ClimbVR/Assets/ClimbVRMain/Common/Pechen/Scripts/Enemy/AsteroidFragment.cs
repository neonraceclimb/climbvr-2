﻿using UnityEngine;

public class AsteroidFragment : MonoBehaviour {

    [SerializeField] private Rigidbody m_Rb;
    //[RangeSlider(0f, 1f)]
    [SerializeField] private Vector2 m_StartSpeed;
    [SerializeField] private float m_DestroyDistnace;
    private Character m_Player;

	// Use this for initialization
	void Start () {
        m_Player = GameManager.Instance.Player;

        float speed = Random.Range(m_StartSpeed.x, m_StartSpeed.y);
        Vector3 force = Vector3.down * speed;

        m_Rb.AddForce(force, ForceMode.Impulse);
        m_Rb.AddTorque(Random.onUnitSphere * speed * 0.003f, ForceMode.Impulse);
	}

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("DebrisVolume"))
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }
}
