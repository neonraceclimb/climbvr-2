﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalLazer : LazerBase {

    public Transform m_Origin;
    public float m_Length;

    [SerializeField]
    protected float m_ActiveTime;
    [SerializeField]
    protected float m_WaitTime;
    [SerializeField]
    protected float m_ReloadTime;

    protected const string m_MatRimCol = "_RimColor";
    protected int m_RimPropID;

    [SerializeField] protected MeshRenderer[] m_Fires;

    protected override void Start()
    {
        base.Start();
        m_RimPropID = Shader.PropertyToID(m_MatRimCol);

        StartCoroutine(Barier());
    }

    protected virtual IEnumerator Barier()
    {
        while (true)
        {
            yield return StartCoroutine(Reload(true));

            yield return StartCoroutine(Active(m_Length));

            yield return StartCoroutine(Reload(false));

            yield return StartCoroutine(Wait());
        }
    }

    protected IEnumerator Reload(bool reload)
    {
        int i = 0;

        //VisualizeRay(BlockBarier1.position, BlockBarier2.position, 0.05f);
        while (i < m_Fires.Length)
        {
            EnableFire(i, reload);

            i++;
            yield return new WaitForSeconds(m_ReloadTime);
        }
        DisableVisualization();
    }

    protected void EnableFire(int i, bool reload)
    {
        if (i == 0)
        {
            for (int j = 0; j < m_Fires.Length; j++)
            {
                m_Fires[j].material.SetColor(m_RimPropID, Color.white);
            }
        }

        if(reload)
        {
            m_Fires[i].material.SetColor(m_RimPropID, Color.yellow);
        }
    }

    protected virtual IEnumerator Active(float distance)
    {
        float t = 0f;
        EnableFire(0, false);

        while (t <= m_ActiveTime)
        {
            Vector3? intersection = CastLazer(m_Origin.position, m_Origin.position + Vector3.down * m_Length, distance);
            Vector3 endPos = intersection != null ? (Vector3)intersection : m_Origin.position + Vector3.down * m_Length;
            VisualizeRay(m_Origin.position, endPos);

            t += Time.deltaTime;
            yield return null;
        }
        DisableVisualization();
    }

    protected virtual IEnumerator Wait()
    {
        float t = 0f;

        while (t <= m_WaitTime)
        {
            DisableVisualization();

            t += Time.deltaTime;
            yield return null;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(m_Origin.position, m_Origin.position + Vector3.down * m_Length);
    }
}
