﻿using System.Collections;
using UnityEngine;

public class FragmentSpawner : MonoBehaviour {

    [SerializeField] private GameObject m_DebrisPrefab;
    [SerializeField] private float m_InnerRadius;
    [SerializeField] private float m_OuterRadius;
    [SerializeField] private float m_SpawnRate;

    private float m_RingThickness;


    // Use this for initialization
    void Start() {
        StartCoroutine(SpawnFragments(m_SpawnRate));
    }

    private IEnumerator SpawnFragments(float rate)
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(1f / rate);
            Instantiate(m_DebrisPrefab, transform.position + GetRandomRingPoint(), Quaternion.identity, this.transform);
        }
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnDrawGizmos()
    {
        DrawCircle(m_InnerRadius, Color.blue);
        DrawCircle(m_OuterRadius, Color.red);
    }

    private void DrawCircle(float radius, Color color)
    {
        int segments = 40;
        float angle = (360f / segments) * Mathf.Deg2Rad;
        float currentAngle = 0f;
        Vector3 p0 = transform.position + GetCirclePoint(currentAngle, radius);
        Gizmos.color = color;
        for (int i = 0; i < segments; i++)
        {
            currentAngle += angle;
            Vector3 p1 = transform.position + GetCirclePoint(currentAngle, radius);
            Gizmos.DrawLine(p0, p1);
            p0 = p1;
        }
    }

    private Vector3 GetCirclePoint(float angle, float radius)
    {
        return new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)) * radius;
    }

    private Vector3 GetRandomRingPoint()
    {
        float angle = Random.Range(0f, Mathf.PI * 2f);

        Vector3 p0 = GetCirclePoint(angle, m_InnerRadius);
        Vector3 p1 = GetCirclePoint(angle, m_OuterRadius);

        return Vector3.Lerp(p0, p1, Random.value);
    }
}
