﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBase : MonoBehaviour
{

    [SerializeField] private LayerMask m_RayMask;
    [SerializeField] private LineRenderer m_LazerRenderer;

    protected Character m_Player;

    protected virtual void Start()
    {
        m_Player = GameManager.Instance.Player;
    }

    protected Vector3? CastLazer(Vector3 from, Vector3 to, float distance)
    {
        Ray ray = new Ray(from, (to - from).normalized);
        Debug.DrawLine(from, to, Color.red);
        RaycastHit hit;
        Vector3? intersect = null;
        if (Physics.Raycast(ray, out hit, distance, m_RayMask))
        {
            intersect = hit.point;
            if (hit.collider.CompareTag("Player"))
            {
                m_Player.SetDamage("Lazer");
            }
        }
        //else
        //{
        //    intersect = transform.forward * distance;
        //}

        return intersect;
    }

    protected void VisualizeRay(Vector3 form, Vector3 to, float width = 0.2f)
    {
        if (!m_LazerRenderer.enabled) m_LazerRenderer.enabled = true;

        m_LazerRenderer.SetPosition(0, form);
        m_LazerRenderer.SetPosition(1, to);

        m_LazerRenderer.startWidth = width;
        m_LazerRenderer.endWidth = width;
    }

    protected void DisableVisualization()
    {
        m_LazerRenderer.enabled = false;
    }
}
