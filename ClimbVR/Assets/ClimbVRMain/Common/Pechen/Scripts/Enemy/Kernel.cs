﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kernel : MonoBehaviour {

    private Character m_Player;

	// Use this for initialization
	void Start () {
        m_Player = GameManager.Instance.Player;
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            m_Player.SetDamage("Kernel");
        }
    }
}
