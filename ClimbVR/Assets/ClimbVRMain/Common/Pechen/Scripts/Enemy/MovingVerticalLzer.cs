﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingVerticalLzer : VerticalLazer {

    [SerializeField]
    protected LaserMover_2 m_LazerMover;
    [SerializeField]
    protected bool m_FirstInQueue;
    [SerializeField]
    protected MovingVerticalLzer m_NextInQueue;


    protected override void Start()
    {
        m_Player = GameManager.Instance.Player;

        if (m_FirstInQueue)
        {
            StartCoroutine(Barier());
        }
    }

    protected override IEnumerator Barier()
    {
        yield return StartCoroutine(Reload(true));

        yield return StartCoroutine(Active(m_Length));

        yield return StartCoroutine(Reload(false));

        yield return StartCoroutine(Wait());

        StartCoroutine(m_NextInQueue.Barier());
    }

    protected override IEnumerator Active(float distance)
    {
        float t = 0f;
        m_LazerMover.StartMoving();
        EnableFire(0, false);

        while (t <= m_ActiveTime)
        {
            Vector3? intersection = CastLazer(m_Origin.position, m_Origin.position + Vector3.down * m_Length, distance);
            Vector3 endPos = intersection != null ? (Vector3)intersection : m_Origin.position + Vector3.down * m_Length;
            VisualizeRay(m_Origin.position, intersection != null ? (Vector3)intersection : m_Origin.position + Vector3.down * m_Length);

            t += Time.deltaTime;
            yield return null;
        }

        m_LazerMover.StopMoving();
        while (m_LazerMover.IsMoving)
        {
            Vector3? intersection = CastLazer(m_Origin.position, m_Origin.position + Vector3.down * m_Length, distance);
            Vector3 endPos = intersection != null ? (Vector3)intersection : m_Origin.position + Vector3.down * m_Length;
            VisualizeRay(m_Origin.position, intersection != null ? (Vector3)intersection : m_Origin.position + Vector3.down * m_Length);
            yield return null;
        }
        DisableVisualization();
    }
}
