﻿Shader "Unlit/HologramDistortionNoGlitchHor"
{
	Properties
	{
		_Tint("Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Texture", 2D) = "gray" {}
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimSize("Rim Size", Range(.5, 3)) = 1
		_LinesFriquency("Line Friquency", Range(0.001, 45)) = 20
		_LinesSpeed("Lines Speed", Float) = 1
		_BigLinesFriquency("Big Line Friquency", Range(0.001, 5)) = 1
		_BigLinesSpeed("Big Lines Speed", Float) = 4

	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			ZWrite On
			ColorMask 0
		}

		Pass
		{
			ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 wPos : TEXCOORD1;
				float3 wNormal : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _RimColor;
			float _RimSize;
			float4 _Tint;
			float _LinesFriquency;
			float _BigLinesFriquency;
			float _LinesSpeed;
			float _BigLinesSpeed;
			
			v2f vert (appdata v)
			{
				v2f o;

				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.wNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 tex = tex2D(_MainTex, i.uv) * 0.5;

				float3 viewDir = normalize(_WorldSpaceCameraPos - i.wPos);

				float rim = 1 - dot(viewDir, i.wNormal);
				rim = pow(rim, _RimSize);

				float smalLines = step(0.5, frac(i.wPos.x * _LinesFriquency + _Time.y * _LinesSpeed));
				float bigLine = frac(i.wPos.x * _BigLinesFriquency  - _Time.x * _BigLinesSpeed);


				fixed4 col;
				col.rgb = bigLine * _Tint * 0.4 + rim * _RimColor * 2 + tex * _Tint;
				col.a = (smalLines + bigLine + rim) * 0.5;

				return col;
			}
			ENDCG
		}
	}
}
