﻿Shader "Unlit/HologramDistortion"
{
	Properties
	{
		_Tint("Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Texture", 2D) = "gray" {}
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimSize("Rim Size", Range(.5, 3)) = 1
		_LinesFriquency("Line Friquency", Range(0.001, 45)) = 20
		_BigLinesFriquency("Line Friquency", Range(0.001, 5)) = 1

	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			ZWrite On
			ColorMask 0
		}

		Pass
		{
			ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 wPos : TEXCOORD1;
				float3 wNormal : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _RimColor;
			float _RimSize;
			float4 _Tint;
			float _LinesFriquency;
			float _BigLinesFriquency;
			
			v2f vert (appdata v)
			{
				v2f o;

				float gTime = step(0.99, _SinTime.w);
				float gPos = v.vertex.y + _SinTime.y - 1;
				float gPosC = step(-0.1,gPos) * step(gPos, 0.1);

				v.vertex.xz += gPosC * gTime * 0.2 * _SinTime.z;

				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.wNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 tex = tex2D(_MainTex, i.uv) * 0.5;

				float3 viewDir = normalize(_WorldSpaceCameraPos - i.wPos);

				float rim = 1 - dot(viewDir, i.wNormal);
				rim = pow(rim, _RimSize);

				float smalLines = step(0.5, frac(i.wPos.y * _LinesFriquency + _Time.y));
				float bigLine = frac(i.wPos.y * _BigLinesFriquency  - _Time.x * 4);


				fixed4 col;
				col.rgb = bigLine * _Tint * 0.4 + rim * _RimColor * 2 + tex * _Tint;
				col.a = (smalLines + bigLine + rim) * 0.5;

				return col;
			}
			ENDCG
		}
	}
}
