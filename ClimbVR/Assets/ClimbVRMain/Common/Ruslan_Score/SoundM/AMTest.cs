﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AMTest : MonoBehaviour {

    public void StartJust()
    {
        print("StartJust");
        AudioManager.Instance.Play(NamesAndPaths.PLATFORM_TOUCH, gameObject.transform, false, false, 1f, false);
    }

    public void Stop()
    {
        print("Stop");
    }

    public void PauseListener()
    {
        print("PauseListener");
        AudioManager.Instance.Pause();
    }

    public void ResumeListener()
    {
        print("ResumeListener");
        AudioManager.Instance.Resume();
    }

    public void StartPauseable()
    {
        print("StartPauseable");
        AudioManager.Instance.Play(NamesAndPaths.LEVEL_BACKGROUND_MUSIC, gameObject.transform, true, false, 0f, true);
    }

    public void StopPauseable()
    {
        print("StopPauseable");
    }

    public void PausePausable()
    {
        print("PausePausable");
        AudioManager.Instance.Pause(NamesAndPaths.LEVEL_BACKGROUND_MUSIC);
    }

    public void ResumePauseable()
    {
        print("ResumePauseable");
        AudioManager.Instance.Resume(NamesAndPaths.LEVEL_BACKGROUND_MUSIC);
    }

    public void StartPauseable1()
    {
        print("StartPauseable1");
        AudioManager.Instance.Play(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC, gameObject.transform, true, false, 0f, true);
    }

    public void StopPauseable1()
    {
        print("StopPauseable1");
    }

    public void PausePausable1()
    {
        print("PausePausable1");
        AudioManager.Instance.Pause(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC);
    }

    public void ResumePauseable1()
    {
        print("ResumePauseable1");
        AudioManager.Instance.Resume(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC);
    }


}
