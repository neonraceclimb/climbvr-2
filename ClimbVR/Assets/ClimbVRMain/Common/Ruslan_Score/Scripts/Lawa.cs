﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lawa : MonoBehaviour {


    public float start_lava = 1.45f;
    public float speed_lava = 0.05f;
    [SerializeField] private float m_AttractForce;

    private Character m_Player;

    public static Vector3 LawaForce { get; private set; }

    void Awake()
    {
        GameManager.Instance.SetLava(this);
    }

    void Start () {
        m_Player = GameManager.Instance.Player;
	}
	
	
	void Update () {
		if(m_Player.transform.position.y >= start_lava)
        {
            MoveLava();
        }
        AttractPlayer();

    }

    private void MoveLava()
    {
        transform.Translate(0f, speed_lava*Time.deltaTime, 0f, Space.World);
		transform.Rotate (0f, speed_lava*Time.deltaTime, 0f, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            m_Player.SetDamage("Lawa");
        }
    }

    private void AttractPlayer()
    {
        Vector3 attractDir = (transform.position - m_Player.transform.position).normalized;
        //float forceRelativeToDistance = (1f / (1f + attractDir.magnitude));
        
        LawaForce = new Vector3(0f, attractDir.y * m_AttractForce, 0f);
    }

    public Vector3 GetPos()
    {
		return gameObject.transform.position;
    }
}
