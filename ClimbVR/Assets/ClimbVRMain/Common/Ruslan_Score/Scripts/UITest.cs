﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITest : MonoBehaviour {

	[SerializeField]
	private Text energy;
	[SerializeField]
	private Text score;
	[SerializeField]
	private Text hight;

	void Update () {
        print(energy);
		if(energy != null){energy.text = "Energy : " + GV.energy.ToString("F2");}
		if(score != null){score.text = "Score : " + GV.score.ToString();}
		if(hight != null){hight.text = "Hight : " + GV.hight.ToString ("F2"); }
	}
}
