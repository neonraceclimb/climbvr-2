﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DefaultLoadSettings{
//    [SerializeField] private Vector3 defaultLavaPosition;
//    public Vector3 DefaultLavaPosition { get { return defaultLavaPosition; } }
//    [SerializeField] private Vector3 defaultPlayerPosition;
//    public Vector3 DefaultPlayerPosition { get { return defaultPlayerPosition; } }
    [SerializeField, Range(0, 214748364)] private int defaultScoreCount;
    public int DefaultScoreCount { get { return defaultScoreCount; } }
    [SerializeField, Range(0,3)] private int defaultEnergyCount;
    public int DefaultEnergyCount { get { return defaultEnergyCount; } }
}
