﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizmManager :  GenericSingletonClass<PrizmManager> {

    private PlatformBase m_lastPlatform;

    public PlatformBase LastPlatform {
        get { return m_lastPlatform; }
    }

    public Transform LastPrizm {
        get { return m_lastPlatform.transform.parent; }
    }

    public void SetAsLast(PlatformBase platform) {
        m_lastPlatform = platform;
    }
}
