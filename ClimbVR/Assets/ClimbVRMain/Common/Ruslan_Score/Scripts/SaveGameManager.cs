﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameManager : MonoBehaviour {

    public static SaveGameManager Instance;


    public DefaultLoadSettings defSet;

    //test
    //public GameObject testingPlayer;

    private static bool isSave = false;

    // 0,-150,0
    //[SerializeField] private Vector3 lavaFromPlayerOnLoad;
    // or saveYourTransform.Position
	private static Vector3 lavaPos;

	private static Vector3 playerPos;
	private static Vector3 lastSafe;

	private static int score;
    private static int energy;

    //private Character m_Player;


    private static List<PlatformBase> platforms = new List<PlatformBase>();

    // some another platforms
    //private List<TestingPlatforms> testingPlatforms = new List<TestingPlatforms>();


//    private void Start()
//    {
//        m_Player = GameManager.Instance.Player;
//    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
			//GameManager.Instance.Player.SetDamage("Die by space");
            //Load();
        }
#endif
    }
    private void Awake()
    {
		if (Instance == null) { Instance = this; }
		else { Destroy(gameObject); return; }

		DontDestroyOnLoad(gameObject);
    }

    public void Save(Vector3 last)
    {
        isSave = true;
        lastSafe = last;
        playerPos = lastSafe;
        lavaPos = GameManager.Instance.Lava.GetPos();
        score = GV.score;
        energy = GV.energy;
        print("Saving");
    }

    public void AddPlatform(PlatformBase platform)
    {
        platforms.Add(platform);
    }

    //public void TestingPlatforms(TestingPlatforms tp)
    //{
    //    testingPlatforms.Add(tp);
    //}

    public void Load()
    {
        if (isSave)
        {
            SetLava(lavaPos);
            SetPlatforms();
            SetPlayer(playerPos);
            SetScore(score);
			GameManager.Instance.Player.SetEnergy(energy);
			print("End Loading");
        } else
        {
            print("Didn't save");
            SetDefault();
        }
    }

    private void SetLava(Vector3 pos)
    {
        GameManager.Instance.Lava.transform.position = pos;
    }

    private void SetPlatforms()
    {
        foreach (PlatformBase platform in platforms)
        {
            platform.Reset();
        }
        platforms.Clear();
        //print("work");
        //foreach(TestingPlatforms tp in testingPlatforms)
        //{
        //    tp.gameObject.SetActive(true);
        //    tp.TestReset();
        //}
    }

    private void SetPlayer(Vector3 pos)
    {
        GameManager.Instance.Player.transform.position = pos;
		GameManager.Instance.Player.playerSmoothed.transform.position = pos;
		GameManager.Instance.Player.SetFreeMove ();
        //testingPlayer.transform.position = lastSafe.position;
        //testingPlayer.SetActive(true);
    }

    private void SetScore(int score)
    {
        GV.score = score;
    }

    private void SetDefault()
    {
        //SetLava(defSet.DefaultLavaPosition);
        SetPlatforms();
        //SetPlayer(defSet.DefaultPlayerPosition);
        SetScore(defSet.DefaultScoreCount);
		GameManager.Instance.Player.SetEnergy(defSet.DefaultEnergyCount);
		print("Load default");
    }
}
