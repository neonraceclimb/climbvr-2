﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeCube : MonoBehaviour
{

    private Collider player;
    private Character m_Player;

    [SerializeField] private Transform endPoint;
    [SerializeField] private Transform reloadPoint;

    [SerializeField] private float timeOfPlayerMoving;

    [SerializeField] private AnimationCurve curveSlow;
    [SerializeField] private AnimationCurve curveFast;
    [SerializeField] private Easing.Ease m_InSlowTimeEase;
    [SerializeField] private Easing.Ease m_OutSlowTimeEase;
    [SerializeField] private float m_SlowTimeDuration;
    [SerializeField] private float m_EndSlowTimeScale;
    [SerializeField] private float m_EndZoneRadius;


    [SerializeField]
    private float duration = 2f;

    private float time;

    private Vector3 startPos;


    private static bool inCube = false;
    public static bool InCube { get { return inCube; } private set { inCube = value; } }
    private const float m_FixedTimeStep = 0.02f;
    private bool m_IsActive;
    private Vector3 m_EndPoint;
   

    private void Start()
    {
        m_IsActive = true;
        m_Player = GameManager.Instance.Player;
        player = m_Player.GetComponent<Collider>();
        if (timeOfPlayerMoving == 0f) { timeOfPlayerMoving = 5f; }

    }
#if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)){
            m_Player.transform.position = new Vector3(4f, 1f, -5f);
        }  
    }
#endif


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && m_IsActive)
        {
            m_EndPoint = GetNearestPointOnCircle(m_Player.transform.position);
            StartCoroutine(SlowTimeIn());
            startPos = m_Player.transform.position;
            time = 0f;
            inCube = true;
            SaveGameManager.Instance.Save(reloadPoint.transform.position);
            
            foreach(AudioSource source in AudioManager.Instance.Sources)
            {

                if (source.clip.name == NamesAndPaths.LEVEL_BACKGROUND_MUSIC)
                {
                    AudioManager.Instance.Pause(NamesAndPaths.LEVEL_BACKGROUND_MUSIC);
                }

                if (source.clip.name == NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC)
                {
                    AudioManager.Instance.Resume(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC);
                } else { AudioManager.Instance.Play(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC, gameObject.transform, true, false, 0f, true); }

            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && m_IsActive)
        { 
            inCube = false;
            StartCoroutine(SlowTimeOut());

            foreach (AudioSource source in AudioManager.Instance.Sources)
            {
                if (source.clip.name == NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC)
                {
                    AudioManager.Instance.Pause(NamesAndPaths.SAFE_ZONE_BACKGROUND_MUSIC);
                }

                if (source.clip.name == NamesAndPaths.LEVEL_BACKGROUND_MUSIC)
                {
                    AudioManager.Instance.Resume(NamesAndPaths.LEVEL_BACKGROUND_MUSIC);
                } else { AudioManager.Instance.Play(NamesAndPaths.LEVEL_BACKGROUND_MUSIC, gameObject.transform, true, false, 0f, true); }
            }

            m_IsActive = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
		if (other.tag == "Player" && m_IsActive)
        {
            MovePlayer(timeOfPlayerMoving);
            if (!inCube) { inCube = true; }
        }
    }


    private void MovePlayer(float speed)
    {
        time += Time.unscaledDeltaTime / timeOfPlayerMoving;
        m_Player.transform.position = Vector3.Lerp(startPos, m_EndPoint, time);
    }

    private IEnumerator SlowTimeIn()
    {
        float t = 0f;

        float endFixedTimeScale = m_FixedTimeStep * m_EndSlowTimeScale;

        Easing.EaseFunc slowFunc = Easing.GetEase(m_InSlowTimeEase);

        while(t < 1f)
        {
            t += Time.unscaledDeltaTime / m_SlowTimeDuration;

            float easeVal = slowFunc(t);
            Time.timeScale = m_EndSlowTimeScale + (1f - easeVal) * (1f - m_EndSlowTimeScale);
            Time.fixedDeltaTime = Mathf.Lerp(m_FixedTimeStep, endFixedTimeScale, easeVal);

            yield return null;
        }

        Time.timeScale = m_EndSlowTimeScale;
        Time.fixedDeltaTime = endFixedTimeScale;
    }

    private IEnumerator SlowTimeOut()
    {
        float t = 0f;

        float endFixedTimeScale = m_FixedTimeStep * m_EndSlowTimeScale;

        Easing.EaseFunc slowFunc = Easing.GetEase(m_OutSlowTimeEase);

        while (t < 1f)
        {
            t += Time.unscaledDeltaTime / m_SlowTimeDuration;

            float easeVal = slowFunc(t);
            Time.timeScale = m_EndSlowTimeScale + easeVal * (1f - m_EndSlowTimeScale);
            Time.fixedDeltaTime = Mathf.Lerp(endFixedTimeScale, m_FixedTimeStep, easeVal);

            yield return null;
        }

        Time.timeScale = 1f;
        Time.fixedDeltaTime = m_FixedTimeStep;
    }

    private void OnDrawGizmos()
    {
        float sepAngle = 18f;
        float currentAngle = sepAngle / 2f * Mathf.Deg2Rad;
        Vector3 p1 = m_EndZoneRadius * new Vector3(Mathf.Sin(currentAngle), 0f, Mathf.Cos(currentAngle)) + endPoint.position;
        for (int i = 0; i < 20; i++)
        {
            currentAngle += sepAngle * Mathf.Deg2Rad;
            Vector3 p2 = m_EndZoneRadius * new Vector3(Mathf.Sin(currentAngle), 0f, Mathf.Cos(currentAngle)) + endPoint.position;
            Gizmos.DrawLine(p1, p2);
            p1 = p2;
        }

        Gizmos.DrawSphere(m_EndPoint, 0.2f);
    }

    private Vector3 GetNearestPointOnCircle(Vector3 pos)
    {
        Vector3 point = pos - endPoint.position;

        point.y = 0f;

        return point.normalized * m_EndZoneRadius + endPoint.position;
    }

}
