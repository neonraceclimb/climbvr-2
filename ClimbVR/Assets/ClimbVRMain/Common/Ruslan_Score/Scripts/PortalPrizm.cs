﻿using System.Collections;
using UnityEngine;

public class PortalPrizm : MonoBehaviour {

    // This prizm
    [SerializeField] private GameObject prizm;
    // Where to spawn this prizm
    [SerializeField] private Transform spawn;
    // Where to die this prizm
    [SerializeField] private Transform die;
    // Time before start prizm moving in seconds
    [SerializeField] private float waitForMove;
    // WaitBeforeSpawn
    [SerializeField] private float waitForSpawn;
    // Speed of prizm moving
    [SerializeField] private float speed;


    private Renderer rendererOfPrizm;

    // The Player
    private Rigidbody player;



    // Is this prizm spawned yet
    private bool spawned = false;

    private void Start()
    {
        player = GameManager.Instance.Player.GetComponent<Rigidbody>();
        rendererOfPrizm = prizm.GetComponent<Renderer>();
        rendererOfPrizm.enabled = false;
        StartCoroutine(WaitForSpawnPrizm(waitForSpawn));
    }

    private void SpawnPrizm(Transform location)
    {
        prizm.transform.position = location.position;
        rendererOfPrizm.enabled = true;
        StartCoroutine(WaitForMovePrizm(waitForMove));
    }

    private IEnumerator WaitForMovePrizm(float time)
    {
        float startTime = Time.realtimeSinceStartup;
        while (true)
        {
            yield return new WaitForSecondsRealtime(time);
            float endTime = Time.realtimeSinceStartup - startTime;
            if (endTime >= time) { StartCoroutine(MovingPrizm(speed)); break; }
        }
    }

    private IEnumerator MovingPrizm(float speedOfPlayer)
    {
        Vector3 moveVector = die.position - prizm.transform.position;

        float distance = Vector3.Distance(prizm.transform.position, die.position);

        float tempDistance;

        while (true)
        {
            tempDistance = distance;

            prizm.transform.Translate(moveVector.normalized * speedOfPlayer * Time.unscaledDeltaTime);

            yield return new WaitForEndOfFrame();

            distance = Vector3.Distance(prizm.transform.position, die.position);

            if (tempDistance - distance <= 0 ) { DiePrizm(); break; }
        }
    }

    private IEnumerator WaitForSpawnPrizm(float time)
    {
        float startTime = Time.realtimeSinceStartup;
        while (true)
        {
            //do some code
            yield return new WaitForSecondsRealtime(time);
            float endTime = Time.realtimeSinceStartup - startTime;
            if (endTime >= time) { SpawnPrizm(spawn); break; }
        }
    }


    private void DiePrizm()
    {
        rendererOfPrizm.enabled = false;
        StartCoroutine(WaitForSpawnPrizm(waitForSpawn));
    }

}
