﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NamesAndPaths
{
    //Paths
    public const string RESOURCES_SOUNDS_PATH = "Sounds/";
    //Names
    public const string LEVEL_BACKGROUND_MUSIC = "levelBackground";
    public const string SAFE_ZONE_BACKGROUND_MUSIC = "safeZoneBackground";
    public const string WIN_MENU_BACKGROUND_MUSIC = "winMenuBackground";
    public const string MAIN_MENU_BACKGROUND_MUSIC = "mainMenuBackground";
    public const string PLATFORM_TOUCH = "platformTouch";
    //Trash
    public const string TEST_SOUND = "testSound";
    public const string TEST_TEXT_MUSIC = "textMusicTest";
    public const string TEST_TEXT_MUSIC1 = "textMusicTest1";
}
