﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveInputWrapper : MonoBehaviour {

    [SerializeField] private SteamVR_TrackedObject m_TrackedObj;

    private HandsController m_HandsController;

    private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)m_TrackedObj.index); } }

    private void Start()
    {
        m_HandsController = GetComponent<HandsController>();
    }

    private void Update()
    {

        if (m_TrackedObj.index != SteamVR_TrackedObject.EIndex.None)
        {
            if (Controller.GetHairTriggerDown())
            {
                m_HandsController.PressTriggerDown();
            }
            else if (Controller.GetHairTrigger())
            {
                m_HandsController.PressTrigger();
            }
            else if (Controller.GetHairTriggerUp())
            {
                m_HandsController.PressTriggerUp();
            }
            else if (!Controller.GetHairTrigger())
            {
                m_HandsController.TriggerUp();
            }


            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                m_HandsController.PressAimButtonDown();
            }
            else if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
            {
                m_HandsController.PressAimButton();
            }
            else if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
            {
                m_HandsController.PressAimButtonUp();
            }
            else if (!Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                m_HandsController.AimUp();
            }

            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
            {
                m_HandsController.PressShieldButtonDown();
            }
            else if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
            {
                m_HandsController.PressShieldButtonUp();
            }
        }

        Vector3 localPosition = m_TrackedObj.transform.localPosition;
        Quaternion localRotation = m_TrackedObj.transform.localRotation;
        m_HandsController.TrackPosition(localPosition, localRotation);
    }
}
