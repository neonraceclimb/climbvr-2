﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusInputWrapper : MonoBehaviour {


	[SerializeField] private OculusControllerTracker m_TrackedObj;

    [SerializeField] private OVRInput.Button m_GrabButton;

    [SerializeField] private OVRInput.Button m_AimButton;

    [SerializeField] private OVRInput.Button m_UseShield;

    private HandsController m_HandsController;

    private void Start()
    {
        m_HandsController = GetComponent<HandsController>();
    }

    private void Update () {

        if (OVRInput.GetDown(m_GrabButton))
        {
            m_HandsController.PressTriggerDown();
        }
        else if (OVRInput.Get(m_GrabButton))
        {
            m_HandsController.PressTrigger();
        }
        else if (OVRInput.GetUp(m_GrabButton))
        {
            m_HandsController.PressTriggerUp();
        }
        else if (!OVRInput.Get(m_GrabButton))
        {
            m_HandsController.TriggerUp();
        }


        if (OVRInput.GetDown(m_AimButton))
        {
            m_HandsController.PressAimButtonDown();
        }
        if (OVRInput.Get(m_AimButton))
        {
            m_HandsController.PressAimButton();
        }
        if (OVRInput.GetUp(m_AimButton))
        {
            m_HandsController.PressAimButtonUp();
        }
        if (!OVRInput.Get(m_AimButton))
        {
            m_HandsController.AimUp();
        }

        if (OVRInput.GetDown(m_UseShield))
        {
            m_HandsController.PressShieldButtonDown();
        }
        else if (OVRInput.GetUp(m_UseShield))
        {
            m_HandsController.PressShieldButtonUp();
        }
        if (!OVRInput.Get(m_UseShield))
        {
            m_HandsController.ShieldUp();
        }

        Vector3 localPosition = m_TrackedObj.transform.localPosition;
		Quaternion localRotation = m_TrackedObj.transform.localRotation;
        m_HandsController.TrackPosition(localPosition, localRotation);
    }
}
