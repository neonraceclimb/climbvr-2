﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInputWrapper : MonoBehaviour {

    [SerializeField] private KeyCode m_GrabButton;
    [SerializeField] private KeyCode m_AimButton;
    [SerializeField] private KeyCode m_UseShield;
    [SerializeField] private Vector3 m_HandOffset;
    [SerializeField] private Transform m_DebugTransform;
    [SerializeField] private Camera m_Camera;

    private HandsController m_HandsController;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        m_HandsController = GetComponent<HandsController>();
    }

    void Update () {
        if (Input.GetKeyDown(m_GrabButton))
        {
            m_HandsController.PressTriggerDown();
        }
        else if (Input.GetKey(m_GrabButton))
        {
            m_HandsController.PressTrigger();
        }
        else if (Input.GetKeyUp(m_GrabButton))
        {
            m_HandsController.PressTriggerUp();
        }
        else if (!Input.GetKey(m_GrabButton))
        {
            m_HandsController.TriggerUp();
        }

        if (Input.GetKeyDown(m_AimButton))
        {
            m_HandsController.PressAimButtonDown();
        }
        else if (Input.GetKey(m_AimButton))
        {
            m_HandsController.PressAimButton();
        }
        else if (Input.GetKeyUp(m_AimButton))
        {
            m_HandsController.PressAimButtonUp();
        }
        if (!Input.GetKey(m_AimButton))
        {
            m_HandsController.AimUp();
        }


        if (Input.GetKeyDown(m_UseShield))
        {
            m_HandsController.PressShieldButtonDown();
        }
        else if (Input.GetKeyUp(m_UseShield))
        {
            m_HandsController.PressShieldButtonUp();
        }
        if (!Input.GetKey(m_UseShield))
        {
            m_HandsController.ShieldUp();
        }

        Vector3 playerOffset = GameManager.Instance.Player.transform.position;
        Vector3 position = GameManager.Instance.Player.transform.InverseTransformPoint(m_Camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f)) + m_HandOffset);
        position.z = 0.5f;
        if (m_DebugTransform) m_DebugTransform.position = position;
        //print(GameManager.Instance.Player.transform.InverseTransformPoint(m_Camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f))) + "    " + (m_Camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f))));
        Quaternion rotation = Quaternion.identity;
        m_HandsController.TrackPosition(position, rotation);
    }
}
