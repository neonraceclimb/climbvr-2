﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum HandStates { FreeMove, GrabLedge, Aim, HarpoonFly }

public class HandsController : MonoBehaviour
{
    [SerializeField] private Transform m_SnapPoint;
    [SerializeField] private float m_OverlapSphereRadius;
    [SerializeField] private float m_HandSpeed;
    [SerializeField] private float m_GrabSpeed = 10f;
    [SerializeField] private PlayerAnimation m_HandAnim;

    public SteamVR_Controller.DeviceRelation m_Relation;


    private Character m_Player;
    private Harpoon m_Harpoon;
    private Shield m_Shield;
    private bool m_IsTrackedPos;
    private bool m_AimIsSetup;
    private bool m_IsFree;
    private static bool m_ShieldIsSetup;//shield in any hand
    private bool m_GrabShield;//shild in this hand

    private Coroutine m_HandCoroutine;

    public PlatformBase m_CurrentPlatform { get; private set; }
    public Transform m_CurrentLedge { get; private set; }

    public Vector3 m_ControllerLocalPos { get; private set; }
    public Quaternion m_ControllerLocalRot { get; private set; }

    public Vector3 m_PressedHandLocalPosition { get; private set; }
    public Vector3 m_PressedPlayerPosition { get; private set; }
    public Vector3 m_PressedPlayerLocalPosition { get; private set; }

    public Vector3 m_PreviousPosition { get; private set; }

    private void Awake()
    {
        m_AimIsSetup = false;
        m_IsTrackedPos = true;
        m_IsFree = true;
        m_ShieldIsSetup = false;
    }

    private void Start()
    {
        m_Shield = GetComponent<Shield>();
        m_Harpoon = GetComponent<Harpoon>();
        m_Player = GameManager.Instance.Player;
        Character.HarpoonShot += PressTriggerUp;
    }

    private void OnDisable()
    {
        Character.HarpoonShot -= PressTriggerUp;
    }

    private void LateUpdate()
    {
        m_PreviousPosition = m_ControllerLocalPos;
    }

    public void Vibrate(ushort mcs = 300)
    {
        SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(m_Relation)).TriggerHapticPulse(mcs);
    }

    private bool CheckPlatform()
    {
        Collider[] overlapedColliders = Physics.OverlapSphere(m_SnapPoint.position, m_OverlapSphereRadius);

        List<Collider> possibleColliders = new List<Collider>();

        foreach (Collider col in overlapedColliders)//find free platforms
        {
            if (col.CompareTag("Platform") && col.GetComponent<PlatformBase>().GetFreeLedges().Count != 0)
            {
                possibleColliders.Add(col);
            }
        }

        Collider nearest = null;
        float minDist = Mathf.Infinity;
        foreach (Collider col in possibleColliders)//find nearest platform
        {
            float dist = (transform.position - col.transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                nearest = col;
                minDist = dist;
            }
        }

        if (nearest != null)
        {
            m_CurrentPlatform = nearest.GetComponent<PlatformBase>();
            m_CurrentLedge = m_CurrentPlatform.GetNearestLedge(this.transform);
            return true;
        }

        return false;
    }


    public void PressTriggerDown()
    {
        if (m_Player.m_CurrentState == PlayerStates.Aim)
        {

            if (m_Harpoon.m_TargetTransform != null)
            {
                if (GameManager.CurrentState != GameStates.Game && GameManager.CurrentState != GameStates.Pause)  //for menus
                {
                    m_CurrentPlatform = m_Harpoon.m_TargetTransform.GetComponent<PlatformBase>();
                    m_CurrentLedge = m_CurrentPlatform.transform;
                    m_CurrentPlatform.OnTriggerDown(this);
                    m_Harpoon.SwitchOff();
                    m_Player.SetHarpoonFly();
                }
                else //in game (-or pause)
                {
                    Character.HarpoonShot.Invoke();
                    m_CurrentPlatform = m_Harpoon.m_TargetTransform.GetComponent<PlatformBase>();
                    m_CurrentLedge = m_CurrentPlatform.GetNearestLedge(this.transform);

                    m_Player.MoveWithHarpoon(this, m_Harpoon.m_TargetTransform);
                    m_Harpoon.SwitchOff();
                    //enable magnet ray visual instead of aim ray visual
                }
                m_AimIsSetup = false;

            }
        }
        else if(m_Player.m_CurrentState == PlayerStates.HarpoonFly && m_CurrentPlatform != null && m_CurrentLedge != null)
        {
            GrabPlatform();
            Vibrate(1000);
            m_CurrentPlatform.OnTriggerDown(this);
            m_Player.OnGrabPlatform(this);
            m_HandAnim.SetGrab(true);
        }
        else
        {
            if ((m_Player.m_CurrentState == PlayerStates.FreeMove || m_Player.m_CurrentState == PlayerStates.GrabLedge
                || m_Player.m_CurrentState == PlayerStates.Start) && CheckPlatform() && m_IsFree)
            {
                GrabPlatform();
                Vibrate(1000);
                m_CurrentPlatform.OnTriggerDown(this);
                m_Player.OnGrabPlatform(this);
                m_HandAnim.SetGrab(true);
            }
        }
    }

    public void PressTrigger()
    {
        if (m_CurrentPlatform != null && m_CurrentLedge != null)
        {
            m_CurrentPlatform.OnPressTrigger(this);
            m_Player.OnKeepPlatform(this);
        }
    }

    public void PressTriggerUp()
    {
        if (m_CurrentPlatform != null)
        {
            if (m_Player.m_CurrentState == PlayerStates.HarpoonFly)
            {
                print(2);
                m_Player.StopMoveWithHarpoon();
                m_CurrentPlatform.OnTriggerUp(this);
                m_IsFree = true;
            }
            else
            {
                m_CurrentPlatform.OnTriggerUp(this);
                m_Player.OnReleasePlatform(this);
                m_HandAnim.SetGrab(false);

                ReleasePlatform();
            }
        }
    }

    public void TriggerUp()
    {
        PressTriggerUp();
    }

    public void PressAimButtonDown()
    {
        if ((m_Player.m_CurrentState == PlayerStates.FreeMove || m_Player.m_CurrentState == PlayerStates.GrabLedge || m_Player.m_CurrentState == PlayerStates.Start)
            && GameManager.Instance.Player.HaveEnergy() && m_IsFree)
        {
            m_AimIsSetup = true;
            m_IsFree = false;
            m_Harpoon.SwitchOn(this);
            m_Player.OnAimDown();
            m_HandAnim.SetAim(true);
        } else { AudioManager.Instance.Play("noEnergy", gameObject.transform, false, false); }
    }

    public void PressAimButton()
    {
        if (m_AimIsSetup && m_Player.m_CurrentState == PlayerStates.Aim)
        {
            m_Harpoon.Aim(this);
        }
    }

    public void PressAimButtonUp()
    {
        if (m_AimIsSetup && m_Player.m_CurrentState == PlayerStates.Aim)
        {
            m_Harpoon.SwitchOff();
            m_Player.OnAimUp();
            m_HandAnim.SetAim(false);
            m_AimIsSetup = false;
            m_IsFree = true;
        }
    }

    public void AimUp()
    {
        PressAimButtonUp();
    }


    internal void PressShieldButtonDown()
    {
        if (m_Player.m_CurrentState != PlayerStates.Aim && m_Player.m_CurrentState != PlayerStates.HarpoonFly && m_IsFree && !m_ShieldIsSetup && m_Player.HaveEnergy())
        {
            m_Shield.SetupShield();
            m_HandAnim.SetShield(true);
            m_Player.SetEnergy(-1);
            m_IsFree = false;
            m_ShieldIsSetup = true;
            m_GrabShield = true;
        }  else { AudioManager.Instance.Play("noEnergy", gameObject.transform, false, false); }
    }

    internal void PressShieldButtonUp()
    {
        if(m_GrabShield)
        {
            m_Shield.DisableShield();
            m_HandAnim.SetShield(false);
            m_IsFree = true;
            m_ShieldIsSetup = false;
            m_GrabShield = false;
        }
    }

    internal void ShieldUp()
    {
        if (m_GrabShield)
        {
            PressShieldButtonUp();
        }
    }

    private void GrabPlatform()
    {
        m_PressedHandLocalPosition = m_ControllerLocalPos;
        m_PressedPlayerPosition = m_Player.transform.position;

        transform.parent = m_CurrentLedge.transform;
        if(!m_CurrentPlatform.IsTouched)
        {
            ParticleSpawner.Spawn(m_CurrentLedge.position, m_CurrentLedge.rotation, Effects.Effect_2, m_CurrentPlatform.transform);
        }

        //m_PressedPlayerLocalPosition = m_Player.transform.localPosition;

        m_IsTrackedPos = false;
        m_IsFree = false;
        m_HandCoroutine = StartCoroutine(GrabPlatformSmoothly());
    }

    private void ReleasePlatform()
    {
        if (m_HandCoroutine != null)
        {
            StopCoroutine(m_HandCoroutine);
            m_HandCoroutine = null;
        }

        transform.parent = m_Player.SmoothedTransform;
        m_IsTrackedPos = true;

        m_CurrentLedge = null;
        m_CurrentPlatform = null;

        m_IsFree = true;
    }

    public void TrackPosition(Vector3 localPosition, Quaternion localRotation)
    {
        m_ControllerLocalPos = localPosition;
        m_ControllerLocalRot = localRotation;
        if (m_IsTrackedPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, localPosition, m_HandSpeed * Time.fixedDeltaTime);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, localRotation, m_HandSpeed * Time.fixedDeltaTime);
        }
    }

    private IEnumerator GrabPlatformSmoothly()
    {
        float distance = float.PositiveInfinity;
        while(distance > 0.005f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, m_GrabSpeed * Time.deltaTime);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, m_GrabSpeed * Time.deltaTime);
            distance = (m_CurrentLedge.transform.localPosition - transform.localPosition).magnitude;
            yield return null;
        }

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        StopCoroutine(m_HandCoroutine);
        m_HandCoroutine = null;
    }
}
