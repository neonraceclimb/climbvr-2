﻿using System;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public static Action<Transform> GrabLedge = delegate (Transform ledge) { };
    public static Action<Transform> ReleaseLedge = delegate (Transform ledge) { };
}
