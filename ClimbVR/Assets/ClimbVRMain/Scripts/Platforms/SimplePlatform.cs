﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatform : PlatformBase {

    public override void OnTriggerDown(HandsController controller)
    {
        base.OnTriggerDown(controller);
    }

    public override void OnPressTrigger(HandsController controller)
    {
        base.OnPressTrigger(controller);
		//if(m_Player.TrackedHandsCount == 1)
		//{
		//	  m_Player.SetEnergy(-0.01f * Time.deltaTime);
		//}
    }

    public override void OnTriggerUp(HandsController controller)
    {
        base.OnTriggerUp(controller);
    }
}
