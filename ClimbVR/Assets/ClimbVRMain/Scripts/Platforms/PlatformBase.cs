﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBase : MonoBehaviour {

    [SerializeField]
    protected Transform[] m_Ledges;
	[SerializeField]
    protected int plusScore;

    protected Character m_Player;

    protected bool is_touched = false;
    public bool IsTouched { get { return is_touched; } }

    public virtual void Awake()
    {
        if(!gameObject.CompareTag("Platform"))
        {
            gameObject.tag = "Platform";
        }
    }

    protected virtual void Start()
    {
        m_Player = GameManager.Instance.Player;
    }

    public virtual void OnTriggerDown(HandsController controller)
    {
		if (!is_touched) {
            is_touched = true;
			AddScore (plusScore);
            PrizmManager.Instance.SetAsLast(this);
		}
    }

    public virtual void OnPressTrigger(HandsController controller)
    {

    }

    public virtual void OnTriggerUp(HandsController controller)
    {

    }

    public List<Transform> GetFreeLedges()
    {
        List<Transform> freeLedges = new List<Transform>();

        for (int i = 0; i < m_Ledges.Length; i++)
        {
            if(m_Ledges[i].childCount == 0)
            {
                freeLedges.Add(m_Ledges[i]);
            }
        }

        return freeLedges;
    }

    public Transform GetNearestLedge(Transform controller)
    {
        List<Transform> freeLedges = GetFreeLedges();

        Transform nearestLedge = null;
        float minDist = Mathf.Infinity;

        foreach (Transform ledge in freeLedges)
        {
            float dist = (controller.position - ledge.position).sqrMagnitude;
            if (dist < minDist)
            {
                nearestLedge = ledge;
                minDist = dist;
            }
        }

        return nearestLedge;
    }

    public void Reset()
    {
        is_touched = false;
    }


    private void AddScore(int plusScore)
	{
		GV.score += plusScore;
		//Debug.Log("PlusScore " + plusScore + " and now = " + GV.score);
	}
}
