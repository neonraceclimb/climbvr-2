﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPlatform : PlatformBase {

    private bool IsEmpty = false;

    private Coroutine coroutine;

    [SerializeField]
    private int m_AddEnergy;

    public override void OnTriggerDown(HandsController controller)
    {
        base.OnTriggerDown(controller);
		SetEnergy ();
    }

    public override void OnPressTrigger(HandsController controller)
    {
        base.OnPressTrigger(controller);
    }

    public override void OnTriggerUp(HandsController controller)
    {
        base.OnTriggerUp(controller);
    }


	private void SetEnergy()
	{
		if (!IsEmpty) {
            if(GameManager.Instance.Player.EnergyIsFull())
            {
                print("Energy is full");
            } else
            {
                GameManager.Instance.Player.SetEnergy(m_AddEnergy);
                IsEmpty = true;
            }
		}
		else 
		{
			print ("No more energy here");
		}
	}

}
