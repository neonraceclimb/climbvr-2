﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PlayerStates { FreeMove, GrabLedge, Aim, HarpoonFly, Start }


public class Character : MonoBehaviour
{
    //[SerializeField] private float m_PlayerSpeed;
    [SerializeField] private PlayerSmoothed m_PlayerSmoothed;
    [SerializeField] private HexMover m_PrizmaHexMover;
    [SerializeField] private VisualizeEnergy visualizeLeftEnergy;
    [SerializeField] private VisualizeEnergy visualizeRightEnergy;
    [SerializeField] private AnimationCurve m_HarpoonCurve;
    [SerializeField] private SimpleBlit m_Death;
    [SerializeField] private float m_MaxPushOffForce;
    [SerializeField] private float m_HarpoonSpeed;
    [SerializeField] private float m_PushOffMultiplier;

    public PlayerSmoothed playerSmoothed {get{ return m_PlayerSmoothed; }}
    private Coroutine m_DeathCoroutine;
    private Rigidbody m_Rb;
    private Coroutine m_CurrentCoroutine;
    private List<HandsController> m_TrackedHands = new List<HandsController>();
    public PlayerStates m_CurrentState { get; private set; }
    public int TrackedHandsCount { get { return m_TrackedHands.Count; } }

    public static Action HarpoonShot = () => { };

    private Quaternion m_PrevRotation;
    private Vector3 m_PrevControllerPos;
    private Vector3 m_ExpectedShift;

    private Vector3 m_AdittionalDeltaMovement;

    //private SafeCube safeCube;

    public Transform body
    {
        get { return transform; }
    }

    public Transform SmoothedTransform { get { return m_PlayerSmoothed.transform; } }

    private int energyCount;
    private const int m_MaxEnergy = 3;

	public void SetFreeMove ()
	{
		m_CurrentState = PlayerStates.FreeMove;
	}

    private void Awake()
    {
        //GV.energy = 2;//Cheat
        //energyCount = GV.energy;
        GameManager.Instance.SetPlayer(this);
        m_Rb = GetComponent<Rigidbody>();
        energyCount = GV.energy;

        // add bg music
        if(!SafeCube.InCube)
        {
            AudioManager.Instance.Play(NamesAndPaths.LEVEL_BACKGROUND_MUSIC, gameObject.transform, true, false, 0f, true);
        }
    }

    private void Start()
    {
        m_Death.SetColor(Color.red);
        m_Death.SetDamageEffect(0f);
        m_DeathCoroutine = null;
		//Debug.Log ("GV.energy = " + GV.energy);
        //SetEnergy(0);
        m_AdittionalDeltaMovement = Vector3.zero;


        if (GameManager.CurrentState == GameStates.Game)
        {
            m_CurrentState = PlayerStates.Start;
        }
        else
        {
            m_CurrentState = PlayerStates.FreeMove;
        }

    }

    private void Update()
    {
        if (m_PrizmaHexMover)
        {
            switch (m_CurrentState)
            {
                case PlayerStates.FreeMove:
                    break;
                case PlayerStates.GrabLedge:
                    transform.localPosition += transform.rotation * (CalculatePosition() + m_AdittionalDeltaMovement);
                    //transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + m_ExpectedShift, Time.deltaTime * m_PlayerSpeed);

                    PrizmaMove();
                    break;
                case PlayerStates.Aim:
                    if(m_Rb.isKinematic)
                    {
                        transform.localPosition += transform.rotation * (CalculatePosition() + m_AdittionalDeltaMovement);

                        PrizmaMove();
                    }
                    break;
                case PlayerStates.HarpoonFly:
                    PrizmaMove();
                    break;
                case PlayerStates.Start:
                    break;
            }
        }
    }

    private void LateUpdate()
    {
        m_AdittionalDeltaMovement = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (m_PrizmaHexMover)
        {
            switch (m_CurrentState)
            {
                case PlayerStates.FreeMove:

                    m_Rb.AddForce(HexMover.HexForce + Lawa.LawaForce, ForceMode.Force);

                    PrizmaMoveFixed();

                    break;
                case PlayerStates.GrabLedge:
                    break;
                case PlayerStates.Aim:
                    if (!m_Rb.isKinematic)
                    {
                        PrizmaMoveFixed();
                    }
                    break;
                case PlayerStates.HarpoonFly:
                    break;
                case PlayerStates.Start:
                    break;
            }
        }
    }

    private void PrizmaMove()
    {
        Vector3 positionTempa = transform.position;
        Quaternion rotationTempa = transform.rotation;
        Vector3 rbVelTempa = m_Rb.velocity;
        m_PrizmaHexMover.HexMove(ref positionTempa, ref rotationTempa, ref rbVelTempa, false);
        transform.position = positionTempa;
        transform.rotation = rotationTempa;
    }

    private void PrizmaMoveFixed()
    {
#if UNITY_EDITOR
        Debug.DrawLine(m_Rb.position, m_Rb.position + m_Rb.velocity, Color.blue);
        Debug.DrawLine(m_Rb.position, m_Rb.position + HexMover.HexForce, Color.magenta);
#endif
        Vector3 positionTemp = m_Rb.position;
        Quaternion rotationTemp = m_Rb.rotation;
        Vector3 rbVelTemp = m_Rb.velocity;
        m_PrizmaHexMover.HexMove(ref positionTemp, ref rotationTemp, ref rbVelTemp, true);
        m_Rb.position = positionTemp;
        m_Rb.rotation = rotationTemp;

        m_Rb.velocity = m_Rb.rotation * Quaternion.Inverse(m_PrevRotation) * rbVelTemp;
        m_PrevRotation = m_Rb.rotation;
    }

    public void OnGrabPlatform(HandsController hand)
    {
        EnablePhysics(false);

        m_TrackedHands.Add(hand);

        m_CurrentState = PlayerStates.GrabLedge;

        AudioManager.Instance.Play(NamesAndPaths.PLATFORM_TOUCH, gameObject.transform, false, false, 1f);
    }

    public virtual void OnKeepPlatform(HandsController hand)
    {

    }

    public void OnReleasePlatform(HandsController hand)
    {
        m_TrackedHands.Remove(hand);

        if (m_TrackedHands.Count == 0)
        {
            EnablePhysics(true);

            PushOff();

            m_CurrentState = PlayerStates.FreeMove;
        }
    }

    private void PushOff()
    {
        Vector3 deltaMov = m_PlayerSmoothed.DeltaMovement;

        Vector3 force = deltaMov / Time.fixedDeltaTime * m_PushOffMultiplier;

        force = force.magnitude < m_MaxPushOffForce ? force : force.normalized * m_MaxPushOffForce;

        m_Rb.AddForce(force, ForceMode.Impulse);
    }

    public void OnAimDown()
    {
        m_CurrentState = PlayerStates.Aim;
    }

    public void OnAimUp()
    {
        if(m_Rb.isKinematic)
        {
            m_CurrentState = PlayerStates.GrabLedge;
        }
        else
        {
            m_CurrentState = PlayerStates.FreeMove;
        }
    }

    private Vector3 CalculatePosition()
    {
        Vector3 finalDelta = Vector3.zero;
        if (m_TrackedHands.Count == 1)
        {
            finalDelta = m_TrackedHands[0].m_PreviousPosition - m_TrackedHands[0].m_ControllerLocalPos;

        }
        else if (m_TrackedHands.Count == 2)
        {
            Vector3 relativePos1 = m_TrackedHands[0].m_PreviousPosition - m_TrackedHands[0].m_ControllerLocalPos;
            Vector3 relativePos2 = m_TrackedHands[1].m_PreviousPosition - m_TrackedHands[1].m_ControllerLocalPos;
            finalDelta = relativePos1 * 0.5f + relativePos2 * 0.5f;
        }
        else
        {
            Debug.LogWarning("TrakedHands > 2");
        }

        m_PrevControllerPos = m_TrackedHands[0].m_ControllerLocalPos;

        return finalDelta;
    }

    public void MoveWithHarpoon(HandsController hand, Transform harpoonTarget)
    {
        SetEnergy(-1);

        EnablePhysics(false);

        m_CurrentState = PlayerStates.HarpoonFly;

        m_CurrentCoroutine = StartCoroutine(MoveToLedgeHarpoon(hand, harpoonTarget));
    }

    public void SetHarpoonFly()
    {
        m_CurrentState = PlayerStates.HarpoonFly;
    }

    public void StopMoveWithHarpoon()
    {
        if (m_CurrentCoroutine != null) StopCoroutine(m_CurrentCoroutine);

        EnablePhysics(true);

        PushOff();

        m_CurrentState = PlayerStates.FreeMove;
    }

    private void EnablePhysics(bool enable)
    {
        if (!enable)
        {
            m_Rb.angularVelocity = Vector3.zero;
            m_Rb.velocity = Vector3.zero;
        }
        m_Rb.isKinematic = !enable;
        m_Rb.useGravity = enable;
    }

    private IEnumerator MoveToLedgeHarpoon(HandsController hand, Transform harpoonTarget)//refactor//подлетать ближе, платформа может быть далеко или за углом
    {
        Vector3 startPosition = transform.position;
        Vector3 finalPosition = harpoonTarget.position + (transform.position - hand.transform.position);//add offset

        float t = 0f;
        float distance = Vector3.Distance(finalPosition, transform.position);

        float speed = m_HarpoonSpeed / distance;//move to inspector

        while (t < 1f)
        {
            t += Time.deltaTime * speed;
            hand.Vibrate();
            transform.position = Vector3.Lerp(startPosition, finalPosition, m_HarpoonCurve.Evaluate(t));
            PrizmaMove();//temporarry

            distance = Vector3.Distance(finalPosition, transform.position);

            yield return null;
        }

        //StopMoveWithHarpoon();

        hand.PressTriggerDown();
    }

    public void SetEnergy(int energy)
    {
        energyCount += energy;
        energyCount = Mathf.Clamp(energyCount, 0, 3);

        visualizeLeftEnergy.SetEnergy(energyCount);
        visualizeRightEnergy.SetEnergy(energyCount);

        GV.energy = energyCount;
    }

    public bool HaveEnergy()
    {
        return energyCount > 0;
    }

    public bool EnergyIsFull()
    {
        return energyCount == m_MaxEnergy;
    }

    public void SetDamage(string str)
    {
        print("Kill: " + str);
        if (m_DeathCoroutine == null)
        {
            m_DeathCoroutine = StartCoroutine(DeathCoroutine());
        }
    }

    private IEnumerator DeathCoroutine()
    {
        float t = 0f;
        m_CurrentState = PlayerStates.Start;
        while (t < 0.95f)
        {
            t = Mathf.Lerp(t, 1f, Time.deltaTime * 4f);
            m_Death.SetDamageEffect(t);
            yield return null;
        }

        t = 0f;
        m_Death.SetColor(new Color(0f, 0f, 0f, 1f));
        while (t < 0.95f)
        {
            t = Mathf.Lerp(t, 1f, Time.deltaTime * 1f);
            m_Death.SetDamageEffect(t);
            yield return null;
        }
        GameManager.Instance.LoadRestart();
        //SaveGameManager.Instance.Load();
    }

    public void AddDeltaMovement(Vector3 delta)
    {
        m_AdittionalDeltaMovement += delta;
    }
}
