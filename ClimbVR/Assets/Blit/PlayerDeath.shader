﻿Shader "Hidden/PlayerDeth"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DeathEffect("Death Slider", Range(0, 1)) = 0
		_DeathColor("Death Color", Color) = (1, 0, 0, 1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _DeathEffect;
			float4 _DeathColor;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col = lerp(col, col * _DeathColor, _DeathEffect);
				return col;
			}
			ENDCG
		}
	}
}
