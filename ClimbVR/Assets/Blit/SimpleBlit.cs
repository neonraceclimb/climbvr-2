﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBlit : MonoBehaviour {

    [SerializeField] Material m_EffectMat;
    //[SerializeField, Range(0f, 1f)] private float m_DeathRange;


    private MaterialPropertyBlock m_MBlock;

    private void Start()
    {
        m_MBlock = new MaterialPropertyBlock();
    }

    public void SetDamageEffect(float t)
    {
        //m_MBlock.SetFloat("_DeathEffect", m_DeathRange);
        m_EffectMat.SetFloat("_DeathEffect", t);
    }

    public void SetColor(Color col)
    {
        m_EffectMat.SetColor("_DeathColor", col);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, m_EffectMat);
    }
}
